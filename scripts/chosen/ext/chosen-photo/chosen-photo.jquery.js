/*
 * Chosen jQuery plugin to add an photo for preview to the dropdown items.
 */
(function($) {
    $.fn.chosenPhotoPreview = function(options) {
        return this.each(function() {
            var $select = $(this);
            

            // 2. Execute chosen plugin and get the newly created chosen container.
            $select.chosen(options);
            var $chosen_container = $select.closest('.jwf-control-listbox');
            var chosen_photo = $chosen_container.find(".jwf-chosen-photo");

            // 4. Change image on chosen selected element when form changes.
            $select.change(function() {
                var imgSrc = $select.find('option:selected').attr('data-photo-src') || '';
                chosen_photo.html('<img src="'+imgSrc+'"/>');
            });
            $select.trigger('change');
        });
    };
})(jQuery);
