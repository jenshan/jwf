<?php
class JWFThreeBroadCast{
      private $table = "_3wp_broadcast_broadcastdata";
      function __construct()
      {
          //$this->init();
      }

      public function init()
      {
        if(is_multisite()){
        //  add_filter( 'admin_post_thumbnail_html', array($this,'add_control_for_featured_image'), 10, 2 );
        //  add_action('wp_ajax_setBroadcast_FeaturedImage_Ajax', array($this, 'setBroadcast_FeaturedImage_Ajax'));
        }
      }
      public function setBroadcast_FeaturedImage_Ajax(){
        if(!empty($_REQUEST['pid']) && is_multisite()){
          $pid = $_REQUEST['pid'];
          $_thumbnail_id = $_REQUEST['_thumbnail_id'];
          if(empty($_thumbnail_id)){
            $_thumbnail_id = get_post_meta($pid,'_thumbnail_id',true);
          }
          $msgUpdate = array();
          if(!empty($_thumbnail_id)){
          //  $_wp_attached_file = get_post_meta($_thumbnail_id,'_wp_attached_file',true);
          //  $_wp_attachment_metadata = get_post_meta($_thumbnail_id,'_wp_attachment_metadata',true);
            $file_path       = get_attached_file( $_thumbnail_id );
            $file_url        = wp_get_attachment_url( $_thumbnail_id );
            $file_type_data  = wp_check_filetype( basename( $file_path ), null );
            $file_type       = $file_type_data['type'];

            $siteMainID = get_current_blog_id();
            $JWFCommons = new JWFCommons();
            $siteIDs = $JWFCommons->getBlogList();
            $postIDs = $this->getPostDataFromBlogs($pid,$siteMainID);
            $timeout_seconds = 5;
            foreach($postIDs as $siteID => $postID){
              if($siteID != $siteMainID){

                if(!empty($file_url)){
                  switch_to_blog( $siteID );

                    // Insert media file into uploads directory.
                    $inserted_attachment_id = JWFAttachment::storeMediaFile( $file_path, $file_type,$postID );
                    //restore_current_blog();
                    set_post_thumbnail( $postID, $inserted_attachment_id );

                  //update_post_meta($postID,'_thumbnail_id',$inserted_attachment_id);
                  $msgUpdate[] = "<a href='".get_edit_post_link($postID)."' target='_blank'>".$siteIDs[$siteID]."</a>";
                  restore_current_blog();
                }


              }
          }
          if(!empty($msgUpdate)){
              $msgUpdate = implode(', ',$msgUpdate);
              echo "<p> Updated for ". $msgUpdate."</p>";
          }else{
            echo "<p> Nothing is updated!</p>";
          }
        }
      }
        exit;
      }

      public function add_control_for_featured_image( $content, $post_id ) {
        if(is_multisite()){
          $blogID = get_current_blog_id();
        //  $dbReturnData = $this->getDataLinkedPostFromBlogID($blogID, $post_id);
          if($this->isBlogParent($blogID,$post_id)){
            $field_id    = 'pi_broadcast_button_featured_image_id';
          	$field_id_msg    = 'pi_broadcast_button_featured_image_msg';

          	$field_data = sprintf(
          	    '<div class="%1$s"><a href="#" class="button-primary" id="%1$s" pid="%2$s" title="Broadcast this image quickly to all sites in network">Quick broadcast this image to all sites</a></div>
                <div class="%3$s"></div>',
          	    $field_id, $post_id, $field_id_msg);

          	return $content .= $field_data;
          }
        }
        return $content;

      }


      public function getDataLinkedPostFromBlogID($blogID,$postContentID){
        global $wpdb;
        $bctb = $wpdb->base_prefix.$this->table ;
        //$mydata = $wpdb->get_row($wpdb->prepare("SELECT wpbc_tb.data FROM ".$wpdb->prefix.$this->table." wpbc_tb WHERE ", $blogID, $postID),ARRAY_A);
        $mydata = $wpdb->get_var( $wpdb->prepare(
        	"
        	SELECT      wpbc_tb.data
        	FROM        $bctb wpbc_tb
        	WHERE       wpbc_tb.blog_id = %s AND wpbc_tb.post_id = %s
        	",
          $blogID,
          $postContentID
        ) );
        $wpdb->flush();
        if(!empty($mydata)){
          $mydata = unserialize( base64_decode($mydata));
        }
        return $mydata;
      }
      public function isBlogParent($blogID, $postContentID, $dbReturnData = array()){
        if(empty($dbReturnData)){
          $dbReturnData = $this->getDataLinkedPostFromBlogID($blogID, $postContentID);
        }
        if(isset($dbReturnData["linked_children"]))
        return true;
        else
        return false;
      }

      public function isBlogChild($blogID, $postContentID, $dbReturnData = array()){
        if(empty($dbReturnData)){
          $dbReturnData = $this->getDataLinkedPostFromBlogID($blogID, $postContentID);
        }
        if(isset($dbReturnData["linked_parent"]))
        return true;
        else
        return false;
      }

      public function getPostDataFromBlogs($postContentID,$blogID = null){
          if(empty($blogID)){
            $blogID = get_current_blog_id();
          }
          $dbReturnData = $this->getDataLinkedPostFromBlogID($blogID, $postContentID);
          $blogData = array();
          if(!empty($dbReturnData) && count($dbReturnData) > 0){
            foreach($dbReturnData as $key => $value){
              if($key == "linked_children"){
                  foreach($dbReturnData["linked_children"] as $bid => $pvalue){
                      $blogData[$bid] = intval($pvalue);
                  }
              }
              if($key == "linked_parent"){
                  $bid = $dbReturnData["linked_parent"]["blog_id"];
                  $pvalue = $dbReturnData["linked_parent"]["post_id"];
                  $blogData[$bid] = $pvalue;
              }
            }
          }
          //var_dump($dbReturnData);
          return $blogData;
      }

      public function getBroadcastLinkingByPostID($contentID){
        $siteData = array();
        $JWFCommons = new JWFCommons();
        $currentBlogID = get_current_blog_id();
        $postIDs = $this->getPostDataFromBlogs($contentID,$currentBlogID);
        foreach ($postIDs as $siteID => $postID){
          switch_to_blog( $siteID );
          $postSiteID = get_post_meta($postID,'_jwf_siteid_broadcast',true);
          restore_current_blog();
          $siteData[$siteID] = true;
          if($postSiteID != $currentBlogID){
            $siteData[$siteID] = false;
          }
        }
        return $siteData;
      }
      public function checkFieldDataFromSites($blogID,$contentID,$keyArray = array()){
        $siteData = array();
        $JWFCommons = new JWFCommons();
        $siteIDs = $JWFCommons->getBlogIDList();
        $postIDs = $this->getPostDataFromBlogs($contentID,$blogID);
        if(empty($keyArray) || !is_array($keyArray)){ $keyArray = array(); }
        if(is_multisite() &&  !empty($keyArray)){
          foreach($keyArray as $key){
            $keyValueOrg[$key] = get_post_meta($contentID,$key,true);
          }
          foreach($siteIDs as $siteID){
            if($siteID != $siteMainID){
              switch_to_blog( $siteID );
                $isTheSame = true;
                $listKeyNotTheSame = array();
                foreach($keyArray as $key){
                  if(!empty($postIDs[$siteID])){
                    $keyValue = get_post_meta($postIDs[$siteID],$key,true);
                    $listKeyNotTheSame[$key] = true;
                    if($keyValue != $keyValueOrg[$key]){
                      $isTheSame = false;
                      $listKeyNotTheSame[$key] = false;
                    }
                  }else{
                    $isTheSame = false;
                    $listKeyNotTheSame[$key] = false;
                  }
                }
                restore_current_blog();
                $siteData[$siteID]['results'] = $isTheSame;
                $siteData[$siteID]['keys'] = $listKeyNotTheSame;
            }
          }
          return $siteData;
        }
        else{
          return null;
        }
      }

      static public function getValuePostMeta($postId,$key, $singleReturn = true, $currentBlogID = null, $sitePostID = null){
        $custom_field = '';
          $activeBlogId= 1;
          $custom_field = get_post_meta($postId,$key,true);
        if(is_multisite()){
          $JWFThreeBroadCast = new JWFThreeBroadCast();
            if(empty($currentBlogID)) $currentBlogID = get_current_blog_id();
          $activeBlogId = $currentBlogID;
          if(empty($sitePostID)) $sitePostID = get_post_meta($postId,'_jwf_siteid_'.$key,true);
					$postIDs = $JWFThreeBroadCast->getPostDataFromBlogs($postId,$currentBlogID);
					//$isBlogChild=$JWFThreeBroadCast->isBlogChild($currentBlogID,$postId);
		      // $data = $JWFThreeBroadCast->getDataLinkedPostFromBlogID($blogID,$contentID);
					if(!empty($sitePostID) && !empty($currentBlogID) && $currentBlogID != $sitePostID ){
            if(isset( $postIDs) && !empty( $postIDs[$sitePostID])){
						switch_to_blog( $sitePostID );
						$postId = $postIDs[$sitePostID];
            $activeBlogId = $sitePostID;
						$custom_field = get_post_meta($postId,$key,true);
            restore_current_blog();
            }
					}
			 }
       if($singleReturn !== true){
         $returnArray = array();
         $returnArray['value'] = $custom_field;
         $returnArray['postId'] = $postId;
         $returnArray['blogId'] = $activeBlogId;
         return $returnArray;
       }
       return $custom_field;
     }

     static public function copyMediaToCurrentSite($attachment_id,$siteSourceID,$postID = ''){
        if ( ! $attachment_id ) {
    			return;
    		}
        if(is_multisite()){
          $timeout_seconds = 5;
          $currentBlogID = get_current_blog_id();
          if($currentBlogID != $siteSourceID){
            switch_to_blog( $siteSourceID );
            $file_path       = get_attached_file( $attachment_id );
            $file_url        = wp_get_attachment_url( $attachment_id );
            $file_type_data  = wp_check_filetype( basename( $file_path ), null );
            $file_type       = $file_type_data['type'];
            restore_current_blog();

            if(!empty($file_url)){

              $sideload_result = JWFAttachment::sideloadMediaFile( $file_url,  $file_type, $timeout_seconds );

              // If an error occurred while trying to sideload the media file; continue to next blog.
        			if ( ! $sideload_result || ! empty( $sideload_result['error'] ) ) {
        				//restore_current_blog();
                  $new_file_path = $file_path;
                  $new_file_type = $file_type;
        			}
              else
              {
        			$new_file_path = $sideload_result['file'];
        			$new_file_type = $sideload_result['type'];
              }
        			// Insert media file into uploads directory.
        			$inserted_attachment_id = JWFAttachment::storeMediaFile( $new_file_path, $new_file_type,$postID );
              //restore_current_blog();
              return $inserted_attachment_id;
            }
          }

        }
        return null;

     }

}
