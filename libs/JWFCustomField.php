<?php
class JWFCustomField {

	public $slug;
	public $slugBox;
	public $nameBox;
	public $positionBox = 'normal';
	public $priorityBox = 'default'; // 'high', 'low', 'default'
	public $label;
	public $placeholder;
	public $unit = ""; // the unit will be display behind control input
	public $post_type;
	public $page_template;
	public $page_template_for_child;
	public $checkbox_value;
	public $control_type = 'textbox'; // just support: textbox, textarea, date, time, listbox, gallery, googlemap
	public $listbox_data = array(); // just used for combobox. They key is label and value is value of item
	public $note;
	public $qtranslate = false;
	public $allow_image = true; // just used for gallery
	public $allow_no_image = false; // just used for gallery
	public $max_image = 0; // just used for gallery. The number is used for limit number of images. Default is 0 with unlimit meaning.
	public $pageID = null;
	public $listbox_items = 0; /// used for listbox_multi to limit selected items. Default is 0.

	private $fieldName = array();
	private $fieldString = '';
	private $fieldDisabled = false;
	private $fieldDisabledString = '';
	private $postID;
	private $post_type_for_meta_box = array();
	private $hasUseTinyMCEPopup = false;
	private $boxFields = array();
	private $blogID = "";
	private $blogPostIDs = array();

	function __construct() {
		  global $post;
		$this->postID =  (empty($post)) ? null : $post->ID;
		if(!empty($_REQUEST['post'])){
			$this->postID  = $_REQUEST['post'];
		}
		if(!empty($_REQUEST['post_ID'])){
			$this->postID  = $_REQUEST['post_ID'];
		}
	//	$contentID = $post->ID;
		if(is_multisite()){
			$this->blogID = get_current_blog_id();
			//$currentBlogID = $blogID
			$JWFThreeBroadCast = new JWFThreeBroadCast();
			$this->blogPostIDs = $JWFThreeBroadCast->getPostDataFromBlogs($this->postID ,$this->blogID);
		}
		if ( is_admin() ) {
			add_action('wp_enqueue_scripts', array($this,'add_admin_enqueues'));
			add_action('save_post', array($this,'jwf_dt_save_custom_fields'));
		}

    }

	public function add_admin_enqueues($hook){
		//wp_enqueue_script( 'tiny_mce_js', get_option( 'siteurl' ) . '/wp-includes/js/tinymce/tinymce.min.js');
		wp_enqueue_script( 'tiny_mce_popup_js', get_option( 'siteurl' ) . '/wp-includes/js/tinymce/tiny_mce_popup.js');
	}
  public function register() {

		 add_action( 'admin_init', array($this,'register_field')) ;
		 //add_action('edit_form_after_title', array($this,'jwf_move_top_field'));
	}

	public function jwf_move_top_field() {

    # Get the globals:
    global $post, $wp_meta_boxes;
//var_dump($wp_meta_boxes);
    # Output the "advanced" meta boxes:

    do_meta_boxes(get_post_type($post), 'jwf-top', $post);

    # Remove the initial "advanced" meta boxes:
    unset($wp_meta_boxes[get_post_type($post)]['jwf-top']);

	}

	public function register_field($post) {

		//$post_types = get_post_types( array('public' => true,'exclude_from_search' => false) );
	$post_type_arr = $this->post_type_for_meta_box;
	if(!empty($post_type_arr) && !empty($this->fieldString)){
		foreach ($post_type_arr as $post_type){
			add_meta_box( $this->slugBox, $this->nameBox, array($this,'jwf_dt_custom_metabox'), $post_type, $this->positionBox, $this->priorityBox);

		}
	}
	}

	public function addFields(){
		global $q_config, $post;
	//	var_dump($post);
		$contentID = absint($this->postID);
		$fieldStringHead = "";
		$languages = (!empty($q_config['enabled_languages'])) ? $q_config['enabled_languages'] : array();
		$flags = (!empty($q_config['flag'])) ? $q_config['flag'] : array();
		$post_type_arr = (!is_array($this->post_type)) ? array($this->post_type) : $this->post_type;
		$post_type_for_meta_box_arr = $this->post_type_for_meta_box;
		$this->post_type_for_meta_box = array_merge($post_type_for_meta_box_arr, $post_type_arr);
		$cur_post_type = get_post_type($contentID);
		if(!empty($_GET["post_type"])) $cur_post_type = $_GET["post_type"];
		if((!empty($cur_post_type) && in_array($cur_post_type,$post_type_arr )) || ($contentID == $pageID)){
			$template = get_post_meta( $contentID, '_wp_page_template', true );

			$templateDB = str_replace('/',"-",$template);
			$templateCFG = array();
			$contentParentID = JWFContent::has_parent_page($contentID);

			$useDataParent = false;
			//$messageDataParent = "";
			$disableControl = "";
		//	var_dump($this->page_template_for_child);
			if(!empty($this->page_template_for_child) && ($contentParentID!== false)){
					 $this->page_template = $this->page_template_for_child;
					 $useDataParent = true;
					 //set disable controls
					 $disableControl = " jwf-disabled ";
				//	 $messageDataParent = "";
				 $this->fieldDisabledString = "<p>This data is got from Parent page.";
				 $this->fieldDisabledString .= " Please go to <a href='".get_edit_post_link( $contentParentID)."#".$this->slugBox."'>".get_the_title($contentParentID)."</a> for updating! </p>";
					 $this->fieldDisabled = true;

			}
			if(!empty($this->page_template)){
					$templateCFG = str_replace('/',"-",$this->page_template);
			}

//echo "Jenshan 2";
			if(empty($this->page_template) || in_array($templateDB,$templateCFG) || ($contentID == $pageID)){
//echo "Jenshan 3";
			//print_r($postmeta);


				$slug = $this->slug;


				$custom_field = get_post_meta($contentID,$slug,true);
				$term_meta_detail = get_post_meta($contentID,$slug."_detail",true);
				if($useDataParent === true){
					//if(empty($custom_field) && empty($term_meta_detail)){
						$custom_field = get_post_meta($contentParentID,$slug,true);
						$term_meta_detail = get_post_meta($contentParentID,$slug."_detail",true);
					//}
				}
				$is_switch_blog = FALSE;
				$alertIconLabel = "";
				$currentBlogID =  1;
				/*if(is_multisite()){
					//$blogID = get_current_blog_id();
					$currentBlogID = $this->blogID;
					$sitePostID = get_post_meta($contentID,'_jwf_siteid_'.$slug,true);
					//$JWFThreeBroadCast = new JWFThreeBroadCast();
					$postIDs = $this->blogPostIDs;

					//$isBlogChild=$JWFThreeBroadCast->isBlogChild($blogID,$contentID);
			//		$data = $JWFThreeBroadCast->getDataLinkedPostFromBlogID($blogID,$contentID);
					if(!empty($sitePostID) && $currentBlogID != $sitePostID && !empty( $postIDs[$sitePostID])){
						//$is_switch_blog = TRUE;
						$contentCurrentID = $contentID;
						$custom_field_current = $custom_field;
						$term_meta_detail_current = $term_meta_detail;

						$is_switch_blog = TRUE;
						switch_to_blog( $sitePostID );
						$contentID = $postIDs[$sitePostID];
						$custom_field = get_post_meta($contentID,$slug,true);
						$term_meta_detail = get_post_meta($contentID,$slug."_detail",true);

						if($custom_field_current !== $custom_field){
							if($term_meta_detail !== $term_meta_detail_current){
								restore_current_blog();
								$custom_field = $custom_field_current;
								$term_meta_detail = $term_meta_detail_current;
								$contentID = $contentCurrentID;
								$is_switch_blog = FALSE;
							}
						}
					}


					//update_post_meta($post->ID, '_jwf_siteid_'.$field, $blogID );
					if ($is_switch_blog === FALSE) {
						$alertIconLabel .='<img width="16" alt="The value of fields are differrent with main site. They seem to be updated manually." src="'.jwf_theme_url( 'images/warning.svg', dirname(__FILE__)).'"/>';
					}
				}*/

				if(!empty($this->label)){
					$fieldStringHead = '<div class="jwf-controls'.$disableControl.'row_'.$this->slug.'"><div class="jwf-controls-label"><label for="'.$this->slug.'">'.$this->label.' '.$alertIconLabel.'</label></div>';
				}
					$this->fieldString .= $fieldStringHead;

				switch ($this->control_type){
					case 'gallery-readonly':
					case 'gallery':
					$this->boxFields[$this->slugBox][]=$slug."_detail";

						$isReadOnly = false;
						//var_dump($this->listbox_data);
						if($this->control_type == "gallery-readonly"){
							$isReadOnly = true;
						}

						$this->hasUseTinyMCEPopup = true;
						$gID = "";
						$gIDs = array();
						$key = $slug;
						$control = "";
						if(!$isReadOnly){
							//$term_meta_detail = get_post_meta($contentID,$key."_detail",true);
							/*$term_meta_site = 0;
							$term_current_site = 0;
							if(is_multisite()){
								$term_meta_site = get_post_meta($contentID,$key."_site",true);
								$term_current_site = get_current_blog_id();
							}*/
						//$term_meta_detail = json_decode($term_meta_detail);
							$gIDContents  = "";
							$gIDLabels  = "";
							$gIDImgs = "";
							if(!empty($term_meta_detail)){
								$gIDs = $term_meta_detail["id"];
								$gIDImgs = $term_meta_detail["img"];
								$gIDContents = $term_meta_detail["content"];
								$gIDLabels = $term_meta_detail["label"];
								$gIDLinks = $term_meta_detail["link"];
								//$gIDthumbs = $term_meta_detail["thumb"];
							}else{
								if(!empty($custom_field)){
									$gIDs = explode(",",$custom_field);
									$gIDContents = array();
									$gIDLabels = array();
									$gIDLinks = array();
								}
							}
						}else{
							$dataReadonly = $this->listbox_data;
							foreach ($dataReadonly as $dataID => $picURL){
								$gIDs[] = $dataID;
								$gIDLinks[] = get_edit_post_link($dataID);
								$gIDThumbs[] = $picURL;
								$gIDLabels[] = get_the_title($dataID);
							}
						}
					//	var_dump($contentID);
						//var_dump($term_meta_detail);

						$control .= "<div class='jwf_gallery_main' maxi='".$this->max_image."' cname='term_meta_".$key."' ckey='".$key."'><div class='jwf_gallery_main_item' id='gallery_".$key."'>";
						if(!empty($gIDs) && is_array($gIDs)){
							foreach ($gIDs as $gIDindex => $gID){

								if(!$isReadOnly){
									$gIDContent = (empty($gIDContents[$gIDindex])) ? "" : stripslashes($gIDContents[$gIDindex]);
								}
								//$hintItem = (empty($gIDLabels[$gIDindex])) ? "this item" : "\"".$gIDLabel."\"";
								$gIDLabel = (empty($gIDLabels[$gIDindex])) ? "" : stripslashes($gIDLabels[$gIDindex]);
								$hintItem = (empty($gIDLabels[$gIDindex])) ? "this item" : "\"".$gIDLabel."\"";
								$gIDLink = (empty($gIDLinks[$gIDindex])) ? "" : stripslashes($gIDLinks[$gIDindex]);
								$gIDImg = (empty($gIDImgs[$gIDindex])) ? "" : stripslashes($gIDImgs[$gIDindex]);
								//$deleteIcon =  jwf_theme_url( 'images/delete24x24.png', dirname(__FILE__));
								if(!$isReadOnly){
									//$isEmptyImg = empty($gIDImgs = $ter)
									if(empty($gIDImg)){
										$attachmentURL = jwf_theme_url( 'images/no-image-default.jpg', dirname(__FILE__));
									}else{
										$attachmentURL = JWFAttachment::getThumbnailByURL($gIDImg,'','', false); // used for store org image
									}
									$attachmentName = basename($gIDImg);
									$attachmentExt = pathinfo($attachmentName, PATHINFO_EXTENSION); // To

									$picURL = jwf_theme_url( 'images/no-image-150.jpg', dirname(__FILE__));
									//var_dump($attachmentExt);
									//var_dump($gIDImg);


								//	var_dump($gIDs);
									switch ($attachmentExt){
										case "mp4":
										case "webm":
											 $picURL =   jwf_theme_url( 'images/mp4.png', dirname(__FILE__) );
											 break;
									 	case "pdf":
											 $picURL =   jwf_theme_url( 'images/pdf.png', dirname(__FILE__) );
											 break;
										case "doc":
										case "xsl":
										case "docx":
 										case "xslx":
 											 $picURL =   jwf_theme_url( 'images/file.png', dirname(__FILE__) );
 											 break;
										default:
													if(!empty($gIDImg)){
														$picURL = JWFAttachment::getThumbnailByURL($gIDImg,150,150, true); // just used for display thumbnail
													}
													/*if($term_meta_site != $term_current_site){
														$gIDImgsAttachment = $gIDImgs[$gIDindex];
														if(!empty($gIDImgsAttachment)){
															$gIDAttachment = JWFAttachment::getImageIDbyURL($gIDImgsAttachment);
															if(!empty($gIDAttachment) && $gIDAttachment != $gID){
																$gID = $gIDAttachment;
																$picURL = JWFAttachment::getThumbnailByImgID($gID,150,150, true);
															}
															else{
																if(!empty($term_meta_site)&& !empty($gID)){
																	$picObj = JWFContent::loadImgFromPrimaryBlog($term_meta_site, $gID,150,150);
																	if(!empty($picObj["thumb"])){
																		$picURL = $picObj["thumb"];
																	}
																}
															}
														}
													}*/
												break;
									}
								}else
								{
									$attachmentExt = 'blocks';
									$attachmentURL = (empty($gIDThumbs[$gIDindex])) ? jwf_theme_url( 'images/no-image-default.jpg', dirname(__FILE__)) : $gIDThumbs[$gIDindex];
								}
							//	$picURL = $attachmentURL;
								$control .= "<div class='jwf_gallery_item'>";
								if(!$isReadOnly){
									$control .= "<a class='jwf_gallery_delete' alt='Delete ".$hintItem."' title='Delete this item'></a>";
									$control .= "<div class='jwf_gallery_updated' id='jwf_gallery_updated_".$key."_".$gID."'></div>";
									$control .= "<div class='jwf_option_update'>";
									if($this->allow_no_image == false){
										$control .= "<a class='jwf_gallery_change' alt='Change another ".$hintItem."' fieldid='".$gID."' metakey='".$key."' title='Change another ".$hintItem."' href='#'></a>";
									}
									$control .= "<a class='jwf_gallery_edit' alt='Update more detail for ".$hintItem."' fieldid='".$gID."' metakey='".$key."' title='Update more detail for ".$hintItem."' href='#'></a>";
									$control .= "</div>";
								}else{
									$control .= "<div class='jwf_option_update'>";
									$control .= "<a class='jwf_gallery_view_external' target='_blank' alt='View more detail for ".$hintItem."' fieldid='".$gID."' metakey='".$key."' title='View more detail for  ".$hintItem."' href='".$gIDLink."'></a>";
									$control .= "</div>";
								}
								$control .= "<img src='".$picURL."'/>";
								if(!$isReadOnly){
								$control .= "<input class='jwf-gallery-img-id' ftype='".$attachmentExt."' furl='".$attachmentURL."' value='".$gID."' type='hidden' name='term_meta_".$key."[id][]' id='term_meta_".$key."_".$gID."_id'/>";
								}
								$control .= "<div class='jwf-gallery-img-label-value' id='term_meta_".$key."_".$gID."_label_value'>".$gIDLabel."</div>";
								if(!$isReadOnly){
									$control .= "<input class='jwf-gallery-img-content' value='".htmlentities($gIDContent)."' type='hidden' name='term_meta_".$key."[content][]' id='term_meta_".$key."_".$gID."_content'/>";
									$control .= "<input class='jwf-gallery-img-label' value='".htmlentities($gIDLabel)."' type='hidden' name='term_meta_".$key."[label][]' id='term_meta_".$key."_".$gID."_label'/>";
									$control .= "<input class='jwf-gallery-img-link' value='".htmlentities($gIDLink)."' type='hidden' name='term_meta_".$key."[link][]' id='term_meta_".$key."_".$gID."_link'/>";
									$control .= "<input class='jwf-gallery-img-url' value='".htmlentities($attachmentURL)."' type='hidden' name='term_meta_".$key."[img][]' id='term_meta_".$key."_".$gID."_img'/>";

								}
								$control .= "</div>";
							}
						}


							$add_menu_function = array();
							if($this->allow_image == true && ($this->max_image == 0 || $this->max_image > count($gIDs))){
								$add_menu_function [] = "<a href='#' class='jwf_gallery_item_add'>Add Media Item</a>";
							}
							if($this->allow_no_image == true && ($this->max_image == 0 || $this->max_image > count($gIDs))){
								$add_menu_function [] = "<a href='#' class='jwf_gallery_item_add_no_img'>Add Text Item</a>";
							}
							$add_menu_function = implode("  |  ",$add_menu_function);
							if(!empty($add_menu_function)){
								$control .= "</div><br style='clear:both;'/><p>".$add_menu_function."</p></div>";
							}else{
								$control .= "</div><br style='clear:both;'/></div>";
							}
							if(!$isReadOnly){
								$control .= '<div id="jwf_gallery_detail_'.$key.'" style="display:none;">';
								$control .= '<br/><div><label><strong>Media</strong></label></div>';
								$control .= '<div id="jwf_gallery_detail_image_url_'.$key.'"></div>';
								$control .= '<br/><div><label for="jwf_gallery_detail_label_'.$key.'"><strong>Label</strong></label></div>';
								$control .= '<input style="width:100%;" id="jwf_gallery_detail_label_'.$key.'"/>';
								$control .= '<br/><div><label for="jwf_gallery_detail_link_'.$key.'"><strong>Link</strong></label></div>';
								$control .= '<input style="width:100%;" id="jwf_gallery_detail_link_'.$key.'"/>';
								$control .= '<br/><div><label for="jwf_gallery_detail_content_'.$key.'"><strong>Content</strong></label></div>';
								$control .= '<textarea class="tinymce-enabled" style="width:100%;height: 180px" autocomplete="off" cols="40" id="jwf_gallery_detail_content_'.$key.'"></textarea>';
								$control .= '<p class="submit" align="center"><input type="submit" cfid="" ckey="'.$key.'" id="jwf-submit-gallery-form-'.$key.'" class="jwf-submit-gallery-form button button-primary" value="Update"></p>';
								$control .= '</div>';
								$control .= "<input class='jwf-gallery-img-link' value='' type='hidden' name='term_meta_".$key."[][]'/>";
							$this->fieldString .= $control;
						}
						break;
					case 'editor':
						$this->fieldString .= '<div id="wp-content-editor-container-'.$this->slug.'" class="wp-editor-container"><textarea class="jwf-editor-control" style="height: 300px" autocomplete="off" cols="40" name="'.$this->slug.'" id="'.$this->slug.'">'.$custom_field.'</textarea></div>';
						break;
					case 'textarea':
						$this->fieldString .= '<p><textarea name="'.$this->slug.'" id="'.$this->slug.'">'.$custom_field.'</textarea></p>';
						break;
					case 'listbox':
					case 'listbox_multi':
					case 'listbox_photo':
						$chosen_type = "chosen-select";
						if($this->control_type == "listbox_photo"){
							$chosen_type = "chosen-select-photo";
						}
						$is_multiple = "";
						$selectedValue = is_array($custom_field) ? $custom_field : array($custom_field);
					//	var_dump($selectedValue);
					if(is_multisite()){
						$currentBlogID = $this->blogID;
						$sitePostID = get_post_meta($contentID,'_jwf_siteid_'.$slug,true);
						$JWFThreeBroadCast = new JWFThreeBroadCast();
						if(!empty($selectedValue)){
							foreach($selectedValue as $keys=>$value){
								if(is_numeric($value)){
									$postExIDs = $JWFThreeBroadCast->getPostDataFromBlogs($value,$sitePostID);
									//var_dump($postExIDs);
									if(!empty($postExIDs[$currentBlogID])){
										$selectedValue[$keys] = $postExIDs[$currentBlogID];
									}
								}
							}
						}
					}
						$slug_id = $slug;
						$selectedValueOrdered = "";
						if($this->control_type == "listbox_multi"){
							$is_multiple = " multiple ";
							$slug_ordered = "_".$slug;
							$slug_limit = "_".$slug;
							$slug = $slug."[]";

							$selectedValueOrdered = implode(',',$selectedValue);
							//$selectedValue = is_array($custom_field) ? $custom_field : $selectedValue;
						}

						if(!empty($this->listbox_data)){

							if(!empty($selectedValue)){
								$newlistboxdata = array();
								foreach($selectedValue as $value){
									if(!empty($value) && !empty($this->listbox_data[$value])){
										 $key = $this->listbox_data[$value];
										 $newlistboxdata[$value] = $key;
										 unset($this->listbox_data[$value]);
									}
								}
								if(!empty($newlistboxdata)){
									$this->listbox_data = $newlistboxdata + $this->listbox_data;
								}
							}

							if(empty($this->placeholder)) $this->placeholder = "Please select ...";
							$limited_items = "";
							$this->fieldString .= '<div class="jwf-control-listbox">';
							if($this->control_type == "listbox_multi"){
								$this->fieldString .= ' <input type="hidden" id="'.$slug_ordered.'" name="'.$slug_ordered.'" value="'.$selectedValueOrdered.'">';

								if(!empty($this->listbox_items) && is_numeric($this->listbox_items)){
									$limited_items = "limited = ".$this->listbox_items;
								}
							}
							$this->fieldString .= '<select '.$limited_items.' name="'.$slug.'" id="'.$slug_id.'" data-placeholder="'.$this->placeholder.'" '.$is_multiple.' class="'.$chosen_type.'">';

							if($this->control_type == "listbox_photo"){
								$chosen_type = "chosen-select-photo";
								foreach ($this->listbox_data as $value){
									// 0: name , 1: file, 2: photo
									$selected = !in_array($value[1],$selectedValue) ? "" : ' selected="selected"';

									$this->fieldString .= '<option data-photo-src="'.$value[2].'" value="'.$value[1].'"'.$selected.'>'.$value[0].'</option>';
								}
							}else{
								foreach ($this->listbox_data as $value => $key){
									$selected = !in_array($value,$selectedValue) ? "" : ' selected="selected"';
									$this->fieldString .= '<option value="'.$value.'"'.$selected.'>'.$key.'</option>';
								}
							}
							$this->fieldString .= '</select><div class="jwf-chosen-photo"></div></div>';
						}
						$this->fieldString .= '<p></p>';
						break;
					case 'datetime':
						$this->fieldString .= ' <input type="text" name="'.$this->slug.'" id="'.$this->slug.'" value="'.$custom_field.'" class="jwp-datetimepicker" readonly="readonly" /><p></p>';
						break;
					case 'date':
							$this->fieldString .= ' <input type="text" name="'.$this->slug.'" id="'.$this->slug.'" value="'.$custom_field.'" class="jwp-datepicker" readonly="readonly" /><p></p>';
							break;
					case 'time':
						$this->fieldString .= ' <input type="text" name="'.$this->slug.'" id="'.$this->slug.'" value="'.$custom_field.'" class="jwp-timepicker" readonly="readonly" /><p></p>';
						break;
					case 'checkbox':
						$checked = '';
						if($this->checkbox_value == $custom_field){
							$checked = " checked";
						}
						$this->fieldString .= '  <input style=""'.$checked.' type="checkbox" name="'.$slug.'" id="'.$slug.'" value="'.$this->checkbox_value.'"/><label for="'.$slug.'">'.$this->checkbox_value.'</label><p></p>';
						break;
					case 'checkboxlist':
						$arrCheckValues = explode(',',$custom_field);
						$this->fieldString .= '<br/><br/><div id="'.$slug.'-all" class="wp-tab-panel">';
						if(!empty($this->listbox_data)){
							$this->fieldString .= '<ul class="jwf-checklistbox-control-main categorychecklist form-no-clear">';
							foreach ($this->listbox_data as $id => $label){
								$checked = '';
								if(in_array($id,$arrCheckValues)){
									$checked = " checked";
								}
								$this->fieldString .= '<li><label for="'.$slug.'_'.$id.'"><input class="jwf-checklistbox-control" '.$checked.' type="checkbox" rel="'.$slug.'" id="'.$slug.'_'.$id.'" value="'.$id.'"/>'.$label.'</label></li>';
							}
							$this->fieldString .= '</ul>';
							$this->fieldString .= '<p><input type="hidden" name="'.$slug.'" id="'.$slug.'" value="'.$custom_field.'"/></p>';
						}
						$this->fieldString .= '</div>';
						break;
					case 'googlemap':
							//$custom_field_json = json_decode($custom_field,JSON_UNESCAPED_UNICODE);
							//print_r($custom_field_json );
							$address = $custom_field["address"];
							$lat = $custom_field['lat'];
							$lng = $custom_field['lng'];
							$this->fieldString .= '<p><input type="text" name="'.$slug.'" id="'.$slug.'" value="'.$address.'" class="large-text" /><br/>';
							$this->fieldString .= '<label>Latitude and Longitude</label> <input type="text" name="'.$slug.'_lat" id="'.$slug.'_lat" value="'.$lat.'" class="medium-text" /> <input type="text" name="'.$slug.'_lng" id="'.$slug.'_lng" value="'.$lng.'" class="medium-text" /></p>';
							break;
				case 'static-content':

								//$this->fieldString .= '<input type="text" name="'.$this->slug.'" id="'.$this->slug.'" value="'.$custom_field.'" class="large-text" />'.$unit.'<p></p>';
								if (!empty($this->note)) {
									$this->fieldString .='<center>'.$this->note.'</center>';
									$this->note = "";
								}
								break;

					default:
						$unit = "";
						if(!empty($this->unit)){
							$unit = "<span>&nbsp;".$this->unit."</span>";
						}
						$this->fieldString .= '<input type="text" name="'.$this->slug.'" id="'.$this->slug.'" value="'.$custom_field.'" class="large-text" />'.$unit.'<p></p>';
						break;
				}

				$this->boxFields[$this->slugBox][]=$slug;
				$this->fieldName[] = array($this->slug,$this->control_type);



				if (!empty($this->note)) {
					$this->fieldString .='<p><small><i>'.$this->note.'</i></small></p>';
				}



				if($is_switch_blog !== FALSE){
					restore_current_blog();
				}
				if(!empty($fieldStringHead)){
					$this->fieldString .='</div>';
				}
			}
		}
		$this->note = "";
		$this->allow_no_image = false;
		$this->page_template = "";
		$this->page_template_for_child ="";
		$this->max_image = 0;

	}
	function jwf_dt_custom_metabox(){

			if(empty($this->fieldString)) return;
		if(!empty($this->fieldDisabled)){
			$this->fieldString = "<div class='jwf-box-disabled'><div class='jwf-box-info-message'>".$this->fieldDisabledString."</div><div class='jwf-box-html'>".$this->fieldString.'</div></div>';
		}
		echo $this->fieldString;

		//if (class_exists('ThreeWP_Broadcast')){
			/*$ThreeWP_Broadcast = new ThreeWP_Broadcast();
			$data = $ThreeWP_Broadcast->get_post_broadcast_data( get_current_blog_id(), $post_id );
			print_r($data);*/
		/*//}
		$currentBlogID = get_current_blog_id();
		$listKeyOrg = $this->boxFields[$this->slugBox];
		$JWFThreeBroadCast = new JWFThreeBroadCast();
		$checkData = $JWFThreeBroadCast->checkFieldDataFromSites($currentBlogID, $this->postID,$listKeyOrg);
		//echo "Jenshan - <pre> "; print_r($checkData); echo "</pre>";*/
	}
	function jwf_dt_save_custom_fields(){
		global $post;
		//echo "<pre>";print_r( $this->fieldName);
		//exit();
		$this->postID = $post->ID;
	// 	// Here, if ew need it, we can also introduce a user's capabilities checking point
	 if ( ! current_user_can( 'edit_post', $post->ID ) ) return;
  //
  //
	// // At this point, as the official documentation suggests for 'save_post' action,
	// // we can check if the save action is an background 'autosave' action or a real user's submit
	// // and also, if it is just a 'revision' or the real last post version.
  //
	// // We check if it's not an autosave.
	 //if ( wp_is_post_autosave( $post->ID ) ) return;
  //
	// // We check if it's not a revision.
	 //if ( wp_is_post_revision( $post->ID ) ) return;

	 //set featured image for post as attachment if it is got from Media instead of uploading

		if(!empty($this->fieldName)){
			foreach($this->fieldName as $fields){
				if(!empty($fields)){
					$field = $fields[0];
					$ctype = $fields[1];
					$value = $_REQUEST[$field];
					//var_dump( $_REQUEST[$field]);
					switch ($ctype) {
						case "gallery":
								$valueGallery = $_POST["term_meta_".$field];
								if(is_array($valueGallery) &&  isset($valueGallery["id"])){
									update_post_meta($post->ID, $field."_detail", $valueGallery );
								}else{
									update_post_meta($post->ID, $field."_detail", "" );
								}
								if(is_multisite()){
									$blogID = get_current_blog_id();
									update_post_meta($post->ID, '_jwf_siteid_'.$field.'_detail', $blogID);
								}
							break;
						case "googlemap":
							$address = $_POST[$field];
							$lat = $_POST[$field."_lat"];
							$lng = $_POST[$field."_lng"];
							$googleAPI = get_option("jwp_admin_google_map_api");
							if((empty($lat) || empty($lng))&&(!empty($address))){
								$JWFGoogle = new JWFGoogle();
								$JWFGoogle->init($googleAPI);
								$latlong = $JWFGoogle->getLatLongFromAdd($address);
								$lat = (empty($lat)) ? $latlong["lat"] : $lat;
								$lng = (empty($lng)) ? $latlong["long"] : $lng;
							}
							$valuen["address"] = $address;
							$valuen["lat"] = $lat;
							$valuen["lng"] = $lng;
							//$valusjson = json_encode($valuen,JSON_UNESCAPED_UNICODE);
							update_post_meta($post->ID, $field, $valuen );
							//var_dump($valusjson);exit();
							break;
							case "listbox_multi":
							case "listbox":
							if( isset($_POST['_'.$field]) && !empty($_POST['_'.$field]) ){
							     $value = explode(",",$_POST['_'.$field]);
							}elseif( !empty($_POST[$field]) ){
							    $value = $_POST[$field];
							}
//var_dump($value); exit();
									update_post_meta($post->ID, $field, $value );
									break;
						default:
							$value = htmlentities($value,ENT_QUOTES);
							update_post_meta($post->ID, $field, $value );
							break;
					}
					if(is_multisite()){
						$blogID = get_current_blog_id();
						update_post_meta($post->ID, '_jwf_siteid_broadcast', $blogID);
						update_post_meta($post->ID, '_jwf_siteid_'.$field, $blogID);
					}




				}
			}
		}
	}

}
?>
