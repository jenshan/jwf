<?php
require_once ABSPATH . WPINC . '/class-wp-image-editor.php';
require_once ABSPATH . WPINC . '/class-wp-image-editor-gd.php';
class JEJO_WP_Image_Editor extends WP_Image_Editor_GD{

	public function __construct( $file ) {
		parent::__construct($file);		
	}

	/**
	 * Saves current in-memory image to file. Override save funcion
	 *
	 * @since 3.5.0
	 * @access public
	 *
	 * @param string|null $filename
	 * @param string|null $mime_type
	 * @return array|WP_Error {'path'=>string, 'file'=>string, 'width'=>int, 'height'=>int, 'mime-type'=>string}
	 */
	public function save( $filename = null, $mime_type = null ) {
		$saved = $this->_save( $this->image, $filename, $mime_type );		
		return $saved;
	}
}
?>