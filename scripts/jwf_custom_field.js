jQuery(document).ready(function($){

jQuery.fn.jwf_chosen_disabled = function(obj){
	if(obj.closest('.jwf-controls').hasClass('jwf-disabled')){
		obj.prop('disabled', true).trigger("chosen:updated").prop('disabled',false);
	}
}

if(jQuery('.jwp-datepicker').length > 0){
	jQuery('.jwp-datepicker').datetimepicker({
		timepicker:false,
	 format:'Y-m-d',
	 minDate: 0//yesterday is minimum date(for today use 0 or -1970/01/01)
	});
}
if(jQuery('.jwp-datetimepicker').length > 0){
	jQuery('.jwp-datetimepicker').datetimepicker({
	 format:'Y-m-d H:i',
	 minDate: 0//yesterday is minimum date(for today use 0 or -1970/01/01)
	});
}
if(jQuery('.jwp-timepicker').length > 0){
	jQuery('.jwp-timepicker').datetimepicker({
		datepicker:false,
	 format:'H:i',
	 allowTimes:[
  '6:00', '6:30', '7:00', '7:30', '8:00', '8:30', '9:00', '9:30', '10:00','10:30',
	'11:00', '11:30', '12:00', '12:30', '13:00', '13:30', '14:00', '14:30', '15:00','15:30',
	'16:00', '16:30', '17:00', '17:30', '18:00', '18:30', '19:00', '19:30', '20:00','20:30',
	'21:00', '21:30', '22:00', '22:30', '23:00', '23:30'
 ]
	});
}

	/* process for gallery field */
	jQuery(".chosen-select").each(function(){
	if(jQuery(this).attr('limited')){
		var max_select = jQuery(this).attr('limited');
		if(max_select > 0){
			  jQuery(this).chosen({ width: "100%", max_selected_options: max_select });
		}
	}else{
		jQuery(this).chosen({width: "100%"});
	}
	jQuery.fn.jwf_chosen_disabled(jQuery(this));
});
	jQuery(".chosen-select").on('change', function(evt, params) {
		var selectedValue = params.selected;
		var deSelectedValue = params.deselected;

				var thisObjID = jQuery(this).attr("id");
				var listData = [];
				var listDataString = "";
				jQuery("#"+thisObjID+"_chosen").find(".search-choice-close").each(function(){
					var index = jQuery(this).attr('data-option-array-index');
					var valueOption = jQuery("#"+thisObjID+' option').eq(index).val();
					if((valueOption != deSelectedValue) && (deSelectedValue != 'undefined')){
						listData.push(valueOption);
					}
				});
			/*	if(selectedValue !=  'undefined') {
					listData.push(selectedValue);
				}*/
				if(listData.length>0){
					listDataString = listData.join(',');
				}
				jQuery('[id="_'+jQuery(this).attr('id')+'"]').val(listDataString);
		});




//jQuery(".chosen-select").chosen({width: "100%"});
	jQuery(".chosen-select-photo").chosenPhotoPreview({width: "100%"});


	jQuery.fn.jejo_resize_thickbox = function(){
		jQuery(document).find('#TB_window').width(TB_WIDTH).height(TB_HEIGHT).css('margin-left', - TB_WIDTH / 2);
	};
	jQuery.fn.jwf_gallery_delete_click = function(){
		jQuery('.jwf_gallery_delete').click(function(e) {
			jQuery(this).closest(".jwf_gallery_item").fadeOut(1000, function() {jQuery(this).remove()});
			var wrapObj = jQuery(this).closest(".jwf_gallery_main");
			var mainObj = wrapObj.find(".jwf_gallery_main_item");
			var maxiImage = wrapObj.attr("maxi");
			if(maxiImage > 0 && mainObj.size() < maxiImage){
					jQuery(wrapObj).find(".jwf_gallery_item_add").show();
			}
		});
	}
	jQuery.fn.jwf_gallery_edit_click = function(){
		jQuery('.jwf_gallery_edit').click(function(e) {
			var controlName = jQuery(this).closest(".jwf_gallery_main").attr("cname");
			var jwf_gallery_item_obj =	jQuery(this).closest(".jwf_gallery_item");
			var metakey = jQuery(this).attr("metakey");
			var fieldid = jQuery(this).attr("fieldid");
			var contentValue = jwf_gallery_item_obj.find(".jwf-gallery-img-content").val();
			var labelValue = jwf_gallery_item_obj.find(".jwf-gallery-img-label").val();
			var linkValue = jwf_gallery_item_obj.find(".jwf-gallery-img-link").val();
			var imgURL = jwf_gallery_item_obj.find(".jwf-gallery-img-id").attr("furl");
			var ext = jwf_gallery_item_obj.find(".jwf-gallery-img-id").attr("ftype");
			var mediaContent = "";
			if(ext == "mp4" || ext == "webm"){
				mediaContent = '<video width="320" height="240" controls><source src="'+imgURL+'" type="video/mp4">Your browser does not support the video tag.</video>';
			}else{
				if(ext == "doc" || ext == "docx" || ext == "pdf" || ext == "xsl" || ext == "xslx"){
					mediaContent = "<a href='"+imgURL+"' target='_blank'>"+imgURL+"</a>";
				}else{
					mediaContent ='<img src="'+imgURL+'" width="320"/>';
				}
			}
			jQuery("#jwf_gallery_detail_"+metakey+" #jwf_gallery_detail_image_url_"+metakey).html(mediaContent);
			jQuery("#jwf_gallery_detail_"+metakey+" #jwf_gallery_detail_content_"+metakey).val(contentValue);
			jQuery("#jwf_gallery_detail_"+metakey+" #jwf_gallery_detail_label_"+metakey).val(labelValue);
			jQuery("#jwf_gallery_detail_"+metakey+" #jwf_gallery_detail_link_"+metakey).val(linkValue);
			jQuery("#jwf_gallery_detail_"+metakey+" #jwf-submit-gallery-form-"+metakey).attr("cfid",fieldid);
			jQuery("#jwf_gallery_detail_"+metakey+" #jwf-submit-gallery-form-"+metakey).attr("ckey",metakey);
			tb_show("Update detail for image","#TB_inline?width=800&height=600&inlineId=jwf_gallery_detail_"+metakey,null);
			jQuery.fn.jejo_resize_thickbox();
			if ( typeof( tinymce ) == "object"){
				tinymce.EditorManager.execCommand('mceRemoveEditor',true, "jwf_gallery_detail_content_"+metakey);
				tinymce.EditorManager.execCommand('mceAddEditor',true,  "jwf_gallery_detail_content_"+metakey);
				tinymce.get("jwf_gallery_detail_content_"+metakey).setContent(contentValue);

				jQuery("#jwf-submit-gallery-form-"+metakey).click(function(e) {
					var fieldid = jQuery(this).attr("cfid");
					var metakey = jQuery(this).attr("ckey");
					var content = tinymce.get("jwf_gallery_detail_content_"+metakey).getContent();
					var label = jQuery("#jwf_gallery_detail_label_"+metakey).val();
					var link = jQuery("#jwf_gallery_detail_link_"+metakey).val();
					jQuery("#term_meta_"+metakey+"_"+fieldid+"_content").val(content);
					jQuery("#term_meta_"+metakey+"_"+fieldid+"_label").val(label);
					jQuery("#term_meta_"+metakey+"_"+fieldid+"_label_value").html(label);
					jQuery("#term_meta_"+metakey+"_"+fieldid+"_link").val(link);
					//jQuery("#term_meta_"+metakey+"_"+fieldid+"_link_value").html(link);
					jQuery.fn.jejo_gallery_set_notice_update(metakey,fieldid);
					tb_remove();
					return false;
				});
			}
			return false;
		});
	};
	jQuery.fn.jejo_gallery_set_notice_update = function(metakey,fieldid){
		jQuery("#jwf_gallery_updated_"+metakey+"_"+fieldid).show();
		if(jQuery(".row_"+metakey+" .jejo-notice").length == 0){
			jQuery(".row_"+metakey).append('<br/><span class="jejo-notice">You have updated detail for items below. Pls click <strong>Update</strong> button for storing!</span>');
		}
	};
	jQuery.fn.selectEventWPMediaNoImage = function(mainObj, controlName,controlKey) {
		// This will return the selected image from the Media Uploader, the result is an object
			var control = "";
			var unix = Math.round(+new Date()/1000);
			var image_id = "jwf"+unix;
			var ext = "jpg";
			var image_url_thumb = jwf_theme_url + "images/no-image-150.jpg";
			var image_url = jwf_theme_url + "images/no-image-default.jpg";
			control += "<div class='jwf_gallery_item'>";
			control += "<a class='jwf_gallery_delete' alt='Delete this media' title='Delete this media'></a>";
			control += "<div class='jwf_gallery_updated' id='jwf_gallery_updated_"+controlKey+"_"+image_id+"'>&nbsp;</div>";
			control += "<div class='jwf_option_update'><a class='jwf_gallery_edit' alt='Update detail for media' fieldid='"+image_id+"' metakey='"+controlKey+"' title='Update detail for media' href='#'></a>";
			control += "</div><img src='"+image_url_thumb+"'/><input class='jwf-gallery-img-id' value='"+image_id+"' ftype='"+ext+"' furl='"+image_url+"' type='hidden' name='term_meta_"+controlKey+"[id][]' id='term_meta_"+controlKey+"_"+image_id+"_id'/>";
			control += "<div class='jwf-gallery-img-label-value' id='term_meta_"+controlKey+"_"+image_id+"_label_value'></div>";
			control += "<input class='jwf-gallery-img-content' value='' type='hidden' name='term_meta_"+controlKey+"[content][]' id='term_meta_"+controlKey+"_"+image_id+"_content'/>";
			control += "<input class='jwf-gallery-img-label' value='' type='hidden' name='term_meta_"+controlKey+"[label][]' id='term_meta_"+controlKey+"_"+image_id+"_label'/>";
			control += "<input class='jwf-gallery-img-link' value='' type='hidden' name='term_meta_"+controlKey+"[link][]' id='term_meta_"+controlKey+"_"+image_id+"_link'/>";
			control += "</div>";

			mainObj.append(control);
			jQuery.fn.jwf_gallery_edit_click();
			jQuery.fn.jwf_gallery_delete_click();
			var wrapObj = mainObj.closest(".jwf_gallery_main");
			var maxiImage = wrapObj.attr("maxi");
			if(maxiImage > 0 && maxiImage <= mainObj.size()){
					 jQuery(wrapObj).find(".jwf_gallery_item_add").hide();
			}
	};
	jQuery.fn.selectEventWPMedia = function(mediaObj, mainObj, controlName,controlKey) {
		// This will return the selected image from the Media Uploader, the result is an object
		var uploaded_image = mediaObj.state().get('selection');
		// We convert uploaded_image to a JSON object to make accessing it easier
		// Output to the console uploaded_image
		//console.log(uploaded_image);
		uploaded_image.map( function( attachment ) {
			var attachment = attachment.toJSON();
			var image_url = attachment.url;
			var unix = Math.round(+new Date()/1000);
			var image_id = "jwf"+unix;
			//var image_id = attachment.id;
			var ext = image_url.split('.').pop();
			var image_url_thumb="";
			var control = "";
			if(ext == "mp4" || ext == "webm"){
				image_url_thumb = jwf_theme_url + "images/mp4.png";
			}else{
				if(ext == "pdf"){
					image_url_thumb = jwf_theme_url + "images/pdf.png";
				}else{
					if(ext == "doc" || ext == "docx" || ext == "xsl" || ext == "xslx"){
						image_url_thumb = jwf_theme_url + "images/file.png";
					}else{
						if(   attachment.sizes !== undefined ){
							if(   attachment.sizes.thumbnail !== undefined  ){
								image_url_thumb = attachment.sizes.thumbnail.url;
							}else{
									image_url_thumb = attachment.sizes.full.url;
							}
						}else{
							image_url_thumb = attachment.url;
						}
					}
				}
			}
			// Let's assign the url value to the input field

			control += "<div class='jwf_gallery_item'>";
			control += "<a class='jwf_gallery_delete' alt='Delete this media' title='Delete this media'></a>";
			control += "<div class='jwf_gallery_updated' id='jwf_gallery_updated_"+controlKey+"_"+image_id+"'></div>";
			control += "<div class='jwf_option_update'>";
			control += "<a class='jwf_gallery_change' alt='Update another media' fieldid='"+image_id+"' metakey='"+controlKey+"' title='Update another media' href='#'></a>";
			control += "<a class='jwf_gallery_edit' alt='Update detail for media' fieldid='"+image_id+"' metakey='"+controlKey+"' title='Update detail for media' href='#'></a>";
			control += "</div><img src='"+image_url_thumb+"'/><input class='jwf-gallery-img-id' value='"+image_id+"' ftype='"+ext+"' furl='"+image_url+"' type='hidden' name='term_meta_"+controlKey+"[id][]' id='term_meta_"+controlKey+"_"+image_id+"_id'/>";
			control += "<div class='jwf-gallery-img-label-value' id='term_meta_"+controlKey+"_"+image_id+"_label_value'></div>";
			control += "<input class='jwf-gallery-img-content' value='' type='hidden' name='term_meta_"+controlKey+"[content][]' id='term_meta_"+controlKey+"_"+image_id+"_content'/>";
			control += "<input class='jwf-gallery-img-label' value='' type='hidden' name='term_meta_"+controlKey+"[label][]' id='term_meta_"+controlKey+"_"+image_id+"_label'/>";
			control += "<input class='jwf-gallery-img-link' value='' type='hidden' name='term_meta_"+controlKey+"[link][]' id='term_meta_"+controlKey+"_"+image_id+"_link'/>";
			control += "<input class='jwf-gallery-img-url' value='"+image_url+"' type='hidden' name='term_meta_"+controlKey+"[img][]' id='term_meta_"+controlKey+"_"+image_id+"_img'/>";

			control += "</div>";

			mainObj.append(control);
			jQuery.fn.jwf_gallery_edit_click();
			jQuery.fn.jwf_gallery_change_click();
			jQuery.fn.jwf_gallery_delete_click();
			var wrapObj = mainObj.closest(".jwf_gallery_main");
			var maxiImage = wrapObj.attr("maxi");
			if(maxiImage > 0 && maxiImage >= mainObj.size()){
					 jQuery(this).hide();
			}
		});
	};

	jQuery.fn.selectUpdateEventWPMedia = function(mediaObj, mainObj, controlName,controlKey) {
		// This will return the selected image from the Media Uploader, the result is an object
		var uploaded_image = mediaObj.state().get('selection');
		var old_detail = mainObj.find(".jwf-gallery-img-content").val();
		var old_label = mainObj.find(".jwf-gallery-img-label").val();
		var old_link = mainObj.find(".jwf-gallery-img-link").val();
		var old_img = mainObj.find(".jwf-gallery-img-url").val();
		// We convert uploaded_image to a JSON object to make accessing it easier
		// Output to the console uploaded_image
		//console.log(uploaded_image);
		uploaded_image.map( function( attachment ) {
			var attachment = attachment.toJSON();
			var image_url = attachment.url;
			var unix = Math.round(+new Date()/1000);
			var image_id = "jwf"+unix;
			//var image_id = attachment.id;
			var ext = image_url.split('.').pop();
			var image_url_thumb="";
			var control = "";
			if(ext == "mp4" || ext == "webm"){
				image_url_thumb = jwf_theme_url + "images/mp4.png";
			}else{
				if(ext == "pdf"){
					image_url_thumb = jwf_theme_url + "images/pdf.png";
				}else{
					if(ext == "doc" || ext == "docx" || ext == "xsl" || ext == "xslx"){
						image_url_thumb = jwf_theme_url + "images/file.png";
					}else{
						if(   attachment.sizes !== undefined ){
							if(   attachment.sizes.thumbnail !== undefined  ){
								image_url_thumb = attachment.sizes.thumbnail.url;
							}else{
									image_url_thumb = attachment.sizes.full.url;
							}
						}else{
							image_url_thumb = attachment.url;
						}
					}
				}
			}
			// Let's assign the url value to the input field

			control += "<a class='jwf_gallery_delete' alt='Delete this media' title='Delete this media'>&nbsp;</a>";
			control += "<div class='jwf_gallery_updated' id='jwf_gallery_updated_"+controlKey+"_"+image_id+"'>&nbsp;</div>";
			control += "<div class='jwf_option_update'>";
			control += "<a class='jwf_gallery_change' alt='Update another media' fieldid='"+image_id+"' metakey='"+controlKey+"' title='Update another media' href='#'></a>";
			control += "<a class='jwf_gallery_edit' alt='Update detail for media' fieldid='"+image_id+"' metakey='"+controlKey+"' title='Update detail for media' href='#'></a>";
			control += "</div><img src='"+image_url_thumb+"'/><input class='jwf-gallery-img-id' value='"+image_id+"' ftype='"+ext+"' furl='"+image_url+"' type='hidden' name='term_meta_"+controlKey+"[id][]' id='term_meta_"+controlKey+"_"+image_id+"_id'/>";
			control += "<div class='jwf-gallery-img-label-value' id='term_meta_"+controlKey+"_"+image_id+"_label_value'>"+old_label+"'</div>";
			control += "<input class='jwf-gallery-img-content' value='"+old_detail+"' type='hidden' name='term_meta_"+controlKey+"[content][]' id='term_meta_"+controlKey+"_"+image_id+"_content'/>";
			control += "<input class='jwf-gallery-img-label' value='"+old_label+"' type='hidden' name='term_meta_"+controlKey+"[label][]' id='term_meta_"+controlKey+"_"+image_id+"_label'/>";
			control += "<input class='jwf-gallery-img-link' value='"+old_link+"' type='hidden' name='term_meta_"+controlKey+"[link][]' id='term_meta_"+controlKey+"_"+image_id+"_link'/>";
			control += "<input class='jwf-gallery-img-url' value='"+image_url+"' type='hidden' name='term_meta_"+controlKey+"[img][]' id='term_meta_"+controlKey+"_"+image_id+"_img'/>";
			mainObj.html(control);
			jQuery.fn.jwf_gallery_edit_click();
			jQuery.fn.jwf_gallery_change_click();
			jQuery.fn.jwf_gallery_delete_click();
			jQuery.fn.jejo_gallery_set_notice_update(controlKey,image_id);
			var wrapObj = mainObj.closest(".jwf_gallery_main");
			var maxiImage = wrapObj.attr("maxi");
			if(maxiImage > 0 && maxiImage <= mainObj.size()){
					 jQuery(wrapObj).find(".jwf_gallery_item_add").hide();
			}
		});
	};

	jQuery('.jwf_gallery_item_add').click(function(e) {
		var wrapObj = jQuery(this).closest(".jwf_gallery_main");
		var mainObj = wrapObj.find(".jwf_gallery_main_item");
		var maxiImage = wrapObj.attr("maxi");
		if(maxiImage > 0 && maxiImage >= mainObj.size()){
				 jQuery(this).hide();
		}
		var controlName = wrapObj.attr("cname");
		var controlKey = wrapObj.attr("ckey");

        e.preventDefault();
        var image = wp.media({
            title: 'Upload Media',
            // mutiple: true if you want to upload multiple files at once
            multiple: true
        }).open()
        .on('select', function(e){
           jQuery(this).selectEventWPMedia(image, mainObj, controlName,controlKey);
	   });
		return false;
    });
	jQuery.fn.jwf_gallery_change_click = function(){
		jQuery('.jwf_gallery_change').click(function(e) {
			var controlName = jQuery(this).closest(".jwf_gallery_main").attr("cname");
			var controlKey = jQuery(this).closest(".jwf_gallery_main").attr("ckey");
			var mainObj = jQuery(this).closest(".jwf_gallery_item");/* just find the current item and replace all */
			e.preventDefault();
			var image = wp.media({
				title: 'Update another media',
				// mutiple: true if you want to upload multiple files at once
				multiple: true
			}).open()
			.on('select', function(e){
				jQuery(this).selectUpdateEventWPMedia(image, mainObj, controlName,controlKey);
		   });
			return false;
		});
	};

	jQuery('.jwf_gallery_item_add_no_img').click(function(e) {
		var wrapObj = jQuery(this).closest(".jwf_gallery_main");
		var mainObj = wrapObj.find(".jwf_gallery_main_item");
		var maxiImage = wrapObj.attr("maxi");
		if(maxiImage > 0 && maxiImage <= mainObj.size()){
				 jQuery(this).hide();
				 return false;
		}
		var controlName = wrapObj.attr("cname");
		var controlKey = wrapObj.attr("ckey");
    jQuery(this).selectEventWPMediaNoImage(mainObj, controlName,controlKey);
		return false;
    });

	jQuery.fn.jwf_gallery_delete_click();
	jQuery.fn.jwf_gallery_edit_click();
	jQuery.fn.jwf_gallery_change_click();

	jQuery('.jwf-upload-term-image').click(function(e) {
		var mainObj = jQuery(this).closest(".jwf-upload-term-image-wrapper");
		e.preventDefault();
		var image = wp.media({
			title: 'Upload Image',
			// mutiple: true if you want to upload multiple files at once
			multiple: false
		}).open()
		.on('select', function(e){
			var uploaded_image = image.state().get('selection');
			uploaded_image.map( function( attachment ) {
				var image_url = attachment.toJSON().url;
				var unix = Math.round(+new Date()/1000);
				var image_id = "jwf"+unix;

				//var image_id = attachment.toJSON().id;
				// Let's assign the url value to the input field
				mainObj.find(".jwf-upload-term-image-url").val(image_url);
				if(mainObj.find(".jwf-upload-term-image-url-id").length > 0){
					mainObj.find(".jwf-upload-term-image-url-id").val(image_id);
				}
				mainObj.find(".jwf-upload-term-image-display").html('<img src=\"'+image_url+'\" style=\"max-width: 200px;\"/><br/>');
			});
		});
	});

	jQuery(".jwf_gallery_main_item").sortable();
	jQuery(".jwf_gallery_main_item").disableSelection();
	/* process for gallery field */

	jQuery('#jwf-tab-term-meta-wrapper .nav-tab').click(function(e) {
		var href = jQuery(this).attr("href");
		jQuery('.nav-tab').removeClass("nav-tab-active");
		jQuery(".jwf-tab-content").hide();
		jQuery(this).addClass("nav-tab-active");
		jQuery(href).show();
		return false;
	});
	if(jQuery(".jwf-editor-control").length > 0){
		jQuery(".jwf-editor-control").each(function( index ) {
			var theid = jQuery(this).attr("id");
			if ( typeof( tinyMCE ) == "object" && typeof( tinyMCE.execCommand ) == "function" ) {
				tinyMCE.execCommand('mceAddEditor', false, theid);
			}
		});
	}
	jQuery(".jwf-checklistbox-control").click(function (e) {
		var listID = "";
		var slug = jQuery(this).attr("rel");
		 jQuery(this).closest(".jwf-checklistbox-control-main").find(".jwf-checklistbox-control").each(function (e) {
			if(jQuery(this).is(":checked")){
				 if (listID == "") listID = jQuery(this).val();
				 else listID += "," + jQuery(this).val();
			 }
		 });
		 jQuery('#'+slug).val(listID);
	});
	jQuery(".jejo-js-view-content-select").on("change",function (e) {
		var urlLink = jQuery(this).attr("eurl");
		if(urlLink != "" && urlLink != 0){
			jQuery(this).closest("td").find(".jejo-js-view-content-link").attr("src",urlLink);
		}

	});
	// process for button broadcast on featured image meta box
	if(jQuery('#pi_broadcast_button_featured_image_id').length > 0){
		jQuery('#pi_broadcast_button_featured_image_id').on('click',function(e) {
			e.preventDefault();
			var pid = jQuery(this).attr("pid");
			var thumbnail_id = 0;
			if (jQuery('#_thumbnail_id').length > 0){
				 thumbnail_id = jQuery('#_thumbnail_id').val();
			 }
			jQuery('.pi_broadcast_button_featured_image_id').hide();
			jQuery('.pi_broadcast_button_featured_image_msg').html("<p><span class='spinner is-active'>Updating...</span></p>");
			jQuery('.pi_broadcast_button_featured_image_msg').show();
			jQuery.ajax({
					url: 'admin-ajax.php',
					type: 'GET',
				data: {'action': 'setBroadcast_FeaturedImage_Ajax','pid': pid, '_thumbnail_id': thumbnail_id},
				error: function (jqXHR, exception) {
							var msg = 'Uncaught Error.\n' + jqXHR.responseText;
							if (jqXHR.status === 0) {
								msg = 'Not connect.\n Verify Network.';
							} else if (jqXHR.status == 404) {
								msg = 'Requested page not found. [404]';
							} else if (jqXHR.status == 500) {
								msg = 'Internal Server Error [500].';
							} else if (exception === 'parsererror') {
								msg = 'Requested JSON parse failed.';
							} else if (exception === 'timeout') {
								msg = 'Time out error.';
							} else if (exception === 'abort') {
								msg = 'Ajax request aborted.';
							}
							alert(msg);
							jQuery('.pi_broadcast_button_featured_image_msg').html("");
							jQuery('.pi_broadcast_button_featured_image_msg').hide();
							jQuery('.pi_broadcast_button_featured_image_id').show();

				},
				success: function (result) {
					jQuery('.pi_broadcast_button_featured_image_msg').html(result);
					jQuery('.pi_broadcast_button_featured_image_msg').show();
					jQuery('.pi_broadcast_button_featured_image_id').show();
				}
			});
			//return false;
		});
	}

	//Process for disabled customField of child Page

	if (jQuery(".jwf-box-disabled").length > 0) {
		var jwf_disabled_obj = jQuery(".jwf-box-disabled .jwf-box-html");
		jwf_disabled_obj.find('input').each(function(){
			var objdis = jQuery(this);
			objdis.attr({
	      jwfName: objdis.attr('name'),
	    })
	    .removeAttr('name').attr("disabled",'disabled');
		});

		jwf_disabled_obj.find('a').each(function(){
			var objdis = jQuery(this);
			objdis.attr('href','#');
		});
	}

/*
// html for display:
// <div id="img" style="display:none;">
//	 <img src="" id="newimg" class="top"/>
// </div>

if(jQuery(".jwf-box-disabled").length > 0){
		var thisBox = jQuery(this);
		html2canvas(thisBox.find(".jwf-box-html"), {
		 onrendered: function(canvas) {
		 var imgsrc = canvas.toDataURL("image/png");
		 console.log(imgsrc);
		 thisBox.find("#newimg").attr('src',imgsrc);
		 thisBox.find("#img").show();
		}
		});
	}*/
});
