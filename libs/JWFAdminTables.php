<?php
class JWFAdminTables {	
	function __construct() {
		//$this->init();
    } 
    public function init() {            
		// do anything for init		
    }	
	
	public function getList($tablename,$id = 0, $arrayListWhereLike = array(), $arrayListWhere = array(), $paged = '', $rec_limit = 2)
	{
		global $wpdb;
		
		$where = '';
		if($id > 0)
			$where .= " AND id=".$id;	

		if(!empty($arrayListWhere )){
			foreach($arrayListWhere  as $value){
				$key = key($value);
				$value = $value[$key];
				$where .= " AND {$key}{$value}";
			}
		}
		if(!empty($arrayListWhereLike)){
			foreach($arrayListWhereLike as $value){
				$key = key($value);
				$value = $value[$key];
				$where .= " AND {$key} LIKE '%{$value}%'";
			}
		}
		
		$sql=<<<SQL
SELECT 
	COUNT(*) AS total_row 
FROM 
	{$tablename} 
WHERE 
	1 = 1 
	{$where}	
SQL;
			
		$rec_count = $wpdb->get_row($sql);		
		if( !empty($paged) ) {
		   $page = $paged - 1;
		   $offset = $rec_limit * $page ;
		}
		else {
		   $page = 0;
		   $offset = 0;
		}
		$left_rec = $rec_count->total_row - ($page * $rec_limit);
		$last = ceil($rec_count->total_row/$rec_limit); 

		$sql = <<<SQL
SELECT 
	*
FROM
	{$tablename}
WHERE 
	1 = 1 
	{$where}
ORDER BY
	id DESC
LIMIT 
	{$offset}, {$rec_limit}
SQL;
		$results['data'] = $wpdb->get_results($sql);  
		$results['total_item'] = $rec_count->total_row;
		$results['total_paged'] = $last;
		$results['current_page'] = $paged;
		return $results;
	}	
}	
?>