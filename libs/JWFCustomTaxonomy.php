<?php

// use some functions from https://wordpress.org/plugins/taxonomy-metadata/
class JWFCustomTaxonomy {

	public $slug;
	public $code;
	public $name;
	public $menu_name;
	public $only_add_field = false;
	public $show_ui = true;
	public $show_in_menu = true;
	public $show_in_box_editor = true;
	public $isCategory = true;
	public $post_type;
	public $tabs = array(); // the tab control is only display in the edit taxonomy screen (the value is 'id'=>'title')
	public $field_group_title="";

	public $field_key;
	public $field_tab_id = ""; // this field is only used when the $tabs is not empty.
	public $field_label;
	public $field_note = '';
	public $field_type = 'text'; // text, listbox, listboxtax, image, textarea, contentlink
	public $listbox_data = array(); // just used for listbox. The key is label and value is value of item
	public $listbox_tax = ''; // just used for listboxtax. The value will be name of taxonomy
	public $contentlink_data = ''; // just used for listboxtax. The value will be name of content type
	public $show_in_add_screen = false;

	private $addedFields = false;
	private $fieldList = array();
	private $fieldAddString = '';
	private $fieldEditString = '';
	private $hasUseTinyMCEPopup = false;

	function __construct() {
    }

	public function add_admin_enqueues($hook){
	//	wp_enqueue_script( 'tiny_mce_js', get_option( 'siteurl' ) . '/wp-includes/js/tinymce/tinymce.min.js');
		wp_enqueue_script( 'tiny_mce_popup_js', get_option( 'siteurl' ) . '/wp-includes/js/tinymce/tiny_mce_popup.js');
	}

	static public function getTermIdByPageLinking($keyTermMeta, $pageId ) {
		global $wpdb;
		$termId = $wpdb->get_var( "SELECT term_id FROM $wpdb->termmeta where meta_value = '".$pageId."' and meta_key = '".$keyTermMeta."'" );
		return $termId;
	}

	private function getTermList($taxonomy){
		$dataLocationList = array();
		$args = array( 'hide_empty' => 0 );
		$terms = get_terms( $taxonomy, $args );
		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
			 foreach ( $terms as $term ) {
				 $dataLocationList[$term->term_id] = $term->name;
			 }
		}
		return	$dataLocationList;
	}

    public function register() {
		if ( is_admin() ) {

				add_action('wp_enqueue_scripts', array($this,'add_admin_enqueues'));

		}
			add_action( 'init', array($this,'register_taxonomy_type' ));

	}


	function register_taxonomy_type() {
		$fieldList = $this->fieldList;
		$code = $this->code;
		if(empty($this->code)) $code = $this->slug;
		if($this->only_add_field == false){
			$args = array(
					'label' => __( $this->name ),
					'rewrite' => array( 'slug' => $this->slug ),
					'hierarchical' => $this->isCategory,
					'show_ui'               => $this->show_ui,
					'show_admin_column'     => $this->show_in_menu,
					'query_var'             => true
				);

			if($this->show_in_box_editor == false){
				$args['show_admin_column'] = false;
				$args['meta_box_cb'] = false;
			}

			register_taxonomy(
				$code,
				$this->post_type,
				$args
			);
		}
		foreach ($fieldList as $fieldKey => $field){
			//listbox_tax
			if(!empty($field[5])){
				//listbox_data
				$field[4] = $this->getTermList($field[5]);
			}else{
				//content link
				if(!empty($field[6])){
					$field[4] = JWFWebControls::getDataForListbox($field[6]);
				}
			}
			$fieldList[$fieldKey] = $field;
		}
		$this->fieldList = $fieldList;
		//echo "<pre>";print_r($this->fieldList);
		if($this->addedFields){
			add_action( $code.'_add_form_fields', array($this,'echoAddFields'), 10, 2 );
			add_action( $code.'_edit_form_fields', array($this,'echoEditFields'), 10, 2 );
			add_action( 'create_'. $code, array($this,'insertFields'), 10, 2 );
			add_action( 'edited_'. $code, array($this,'saveFields'), 10, 2 );
		 }
	}

	public function addFields(){
		$this->addedFields = true;
		$fieldList = $this->fieldList;
		$fieldList[] = array($this->field_key,$this->field_label,$this->field_note,$this->field_type,$this->listbox_data,$this->listbox_tax,$this->contentlink_data, $this->show_in_add_screen,$this->field_tab_id);
		// reset data
		$this->field_tab_id = '';
		$this->show_in_add_screen = false;
		$this->listbox_data = array();
		$this->listbox_tax = "";
		$this->field_note = "";
		$this->contentlink_data = "";

		// add list field to global
		$this->fieldList = $fieldList;
	}

	public function echoAddFields(){
		$fields = $this->fieldList;
		$jsscript = "";
		foreach($fields as $field){
			$key = $field[0]; // key
			$label = $field[1]; // label
			$type = $field[3];
			$listData = $field[4];
			$show_in_add_screen = $field[7];
			if(!$show_in_add_screen) continue;
			//$control = '<input type="text" name="term_meta_'.$key.'" id="term_meta_'.$key.'" value="">';
			switch ($type){
				case "text":
				default:
					$control = '<input type="text" name="term_meta_'.$key.'" id="term_meta_'.$key.'" value="">';
					break;
				case "textarea":
					$control = '<textarea rows="5" autocomplete="off" cols="40" name="term_meta_'.$key.'" id="term_meta_'.$key.'"></textarea>';
					break;
				case "image":
					$control = '<div id="tax_layout_image_url_'.$key.'">
					</div>
					<input type="hidden" name="term_meta_'.$key.'" id="term_meta_'.$key.'" class="regular-text" value="">
					<input type="button" name="tax_upload_btn_'.$key.'" id="tax_upload_btn_'.$key.'" class="button-secondary" value="Select Image">';

					$jsscript .= "jQuery(document).ready(function($){
					$('#tax_upload_btn_".$key."').click(function(e) {
						e.preventDefault();
						var image = wp.media({
							title: 'Upload Image',
							// mutiple: true if you want to upload multiple files at once
							multiple: false
						}).open()
						.on('select', function(e){
							var uploaded_image = image.state().get('selection').first();
							var image_url = uploaded_image.toJSON().url;
							// Let's assign the url value to the input field
							$('#term_meta_".$key."').val(image_url);
							$('#tax_layout_image_url_".$key."').html('<img src=\"'+image_url+'\" style=\"max-width: 400px;\"/><br/>');
						});
					});
					});";
					break;
				case 'editor':
					$control = "";
					$custom_field = "";
					$control .= '<div id="wp-content-editor-container-'.$key.'" class="wp-core-ui wp-editor-wrap tmce-active"><textarea class="wp-editor-area" style="height: 300px" autocomplete="off" cols="40" name="term_meta_'.$key.'" id="term_meta_'.$key.'">'.$custom_field.'</textarea></div>';
					break;
				case "listbox":
				case "listboxtax":
					$control ="";
					if(!empty($listData)){
						$control = '<select name="term_meta_'.$key.'" id="'.$key.'" class="">';
						foreach ($listData as $keyData => $valueData){
							$control .= '<option value="'.$keyData.'">'.$valueData.'</option>';
						}
						$control .= '</select>';
					}
					break;
				case 'checkboxlist':
				case "checkboxtax":
						$checked = '';
						$arrCheckValues = "";
						$control ="";
						if(!empty($listData)){
							$control .= '<div id="'.$key.'-all" class="wp-tab-panel">';
							$control .= '<ul class="categorychecklist form-no-clear jwf-checklistbox-control-main">';
							foreach ($listData as $id => $title){
								$checked = '';
								$control .= '<li><label for="term_meta_'.$key."_".$id.'"><input class="jwf-checklistbox-control checkboxlistclass-term_meta_'.$key.'" '.$checked.' rel="term_meta_'.$key.'" type="checkbox" id="term_meta_'.$key."_".$id.'" value="'.$id.'"/>'.$title.'</label></li>';
							}
							$control .= '</ul>';
							$control .= '<p><input type="hidden" name="term_meta_'.$key.'" id="term_meta_'.$key.'" value="'.$custom_field.'"/></p>';

						$control .= '</div>';

						}else{
							$control .= '<div id="'.$key.'-all" class="wp-tab-panel"><br/>No Data<br/></div>';
						}
						break;
			}
			$this->fieldAddString .= '<div class="form-field">
				<label for="term_meta_'.$key.'"><strong>'.$label.'</strong></label>'.$control;
			if(!empty($field[2])){ // note
				$this->fieldAddString .= '<p class="description"><small><i>'.$field[2].'</i></small></p>';

			}
			$this->fieldAddString .= '</div>';

		}
		echo "<hr/>";
		echo $this->fieldAddString;
		//wp_enqueue_media();
	}

	public function echoEditFields($term){
		$t_id = $term->term_id;
 		$fields = $this->fieldList;
		$jsscript = "";
		$tabsHTML = "";
		//echo "<pre>";print_r($fields);
		if(!empty($this->tabs)){
			$tabs = $this->tabs;
			$tabsHTML .= '<tr class="form-field">';
			//$tabsHTML .= '<th colspan="2" scope="row" valign="top"><label>'.$this->field_group_title.'</label><p class="description">'.$this->field_group_note.'</p></th>';
			//$tabsHTML .= '</tr><tr class="form-field">';
			$tabsHTML .=  '<td colspan="2" class="jejo-libs-custom-taxonomy">';
			$tabsHTML .=  '<div id="poststuff" class="postbox">';
			$tabsHTML .=  '<h2>'.$this->field_group_title.'</h2>';
			$tabsHTML .=  '<div class="inside">';
			$tabsHTML .=  '<p class="description">'.$this->field_group_note.'</p>';
			$tabsHTML .= '<h3 class="nav-tab-wrapper" id="jwf-tab-term-meta-wrapper">';
			$active = 'nav-tab-active';
			foreach($tabs as $tabKey => $tabValue){
				$tabcode = sanitize_title($tabValue)."-".$tabKey;
				$tabsHTML .='<a href="#'.$tabcode.'" class="nav-tab '.$active.'">'.$tabValue.'</a>';
				$active = "";
			}
			$tabsHTML .= '</h3>';
		}
		$htmlArray = array();
		$htmlText = "";
		$control = "";
		//var_dump($fields);
		foreach($fields as $field){
			$key = $field[0]; // key
			$label = $field[1]; // label
			$type = $field[3];
			$listData = $field[4];
			$field_tab_id = $field[8];
			$control .= '<tr class="form-field">';
			$control .= '<th scope="row" valign="top" class="row_'.$key.'"><label for="term_meta_'.$key.'">'.$label.'</label></th>';
			$control .=  '<td>';
			$term_meta = get_term_meta($t_id, $key, true);
			switch ($type){
				case "text":
				default:
					$control .= '<input type="text" name="term_meta_'.$key.'" id="term_meta_'.$key.'" value="'.sanitize_text_field($term_meta).'">';
					break;
				case "textarea":
					$control .= '<textarea name="term_meta_'.$key.'" id="term_meta_'.$key.'" rows="5" cols="50" class="large-text">'.sanitize_text_field($term_meta).'</textarea>';
					break;
				case "image":
						$term_meta_val = sanitize_text_field($term_meta);
						$term_meta_id = get_term_meta($t_id, $key."_id", true);
						$control .= '<div class="jwf-upload-term-image-wrapper"><div class="jwf-upload-term-image-display">';
						if(!empty($term_meta_val)){
							$control .= '<img src="'.home_url().$term_meta_val.'" style="max-width: 200px;"/><br/>';
						}
						$control .= '</div>
						<input type="hidden" name="term_meta_'.$key.'" id="term_meta_'.$key.'" class="regular-text jwf-upload-term-image-url" value="'.$term_meta_val.'">
						<input type="hidden" name="term_meta_'.$key.'_id" id="term_meta_'.$key.'_id" class="regular-text jwf-upload-term-image-url-id" value="'.$term_meta_id.'">
						<input type="button" name="tax_upload_btn_'.$key.'" id="tax_upload_btn_'.$key.'" class="button-secondary jwf-upload-term-image" value="Select Image"></div>';
					break;
				case 'gallery':
					if ( is_user_logged_in() ) {
						$this->hasUseTinyMCEPopup = true;
					}
					$gID = "";
					$gIDs = array();
					$term_meta_detail = get_term_meta($t_id, $key."_detail", true);
					$term_meta_detail = json_decode($term_meta_detail);
					$gIDContents  = "";
					if(!empty($term_meta_detail)){
						$gIDs = $term_meta_detail->id;
						$gIDContents = $term_meta_detail->content;
					}else{
						if(!empty($term_meta)){
							$gIDs = explode(",",$term_meta);
							$gIDContents = array();
						}
					}
					$control .= "<div class='jwf_gallery_main' cname='term_meta_".$key."' ckey='".$key."'><div class='jwf_gallery_main_item' id='gallery_".$key."'>";
					if(!empty($gIDs) && is_array($gIDs)){
						foreach ($gIDs as $gIDindex => $gID){
							$gIDContent = (empty($gIDContents[$gIDindex])) ? "" : stripslashes($gIDContents[$gIDindex]);
							//$deleteIcon =  jwf_theme_url( 'images/delete24x24.png', dirname(__FILE__));
							$attachmentURL = wp_get_attachment_url($gID);
							$attachmentExt = pathinfo(parse_url($attachmentURL)['path'], PATHINFO_EXTENSION);
							if(in_array($attachmentExt,array("mp4","webm"))){
								$picURL =   jwf_theme_url( 'images/mp4.png', dirname(__FILE__) );
							}else{
								$picURL = JWFAttachment::getThumbnailByImgID($gID,"150","150", true);
							}
							$control .= "<div class='jwf_gallery_item'>";
							$control .= "<a class='jwf_gallery_delete' alt='Delete this media' title='Delete this media'>&nbsp;</a>";
							$control .= "<div class='jwf_gallery_updated' id='jwf_gallery_updated_".$key."_".$gID."'>&nbsp;</div>";
							$control .= "<a class='jwf_gallery_edit' alt='Update detail for media' fieldid='".$gID."' metakey='".$key."' title='Update detail for media' href='#'>&nbsp;</a>";
							$control .= "<img src='".$picURL."'/><input class='jwf-gallery-img-id' ftype='".$attachmentExt."' furl='".$attachmentURL."' value='".$gID."' type='hidden' name='term_meta_".$key."[id][]' id='term_meta_".$key."_".$gID."_id'/>";
							$control .= "<input class='jwf-gallery-img-content' value='".$gIDContent."' type='hidden' name='term_meta_".$key."[content][]' id='term_meta_".$key."_".$gID."_content'/>";
							$control .= "</div>";
						}
					}
					$control .= "</div><br style='clear:both;'/><p><a href='#' class='jwf_gallery_item_add'>Add Media Item</a></p></div>";
					$control .= '<div id="jwf_gallery_detail_'.$key.'" style="display:none;">';
					$control .= '<br/><div><label><strong>Media</strong></label></div>';
					$control .= '<div id="jwf_gallery_detail_image_url_'.$key.'"></div>';
					$control .= '<br/><div><label for="jwf_gallery_detail_content_'.$key.'"><strong>Media Content</strong></label></div>';
					$control .= '<textarea class="tinymce-enabled" style="width:100%;height: 180px" autocomplete="off" cols="40" id="jwf_gallery_detail_content_'.$key.'"></textarea>';
					$control .= '<p class="submit" align="center"><input type="submit" cfid="" ckey="'.$key.'" id="jwf-submit-gallery-form-'.$key.'" class="jwf-submit-gallery-form button button-primary" value="Update"></p>';
					$control .= '</div>';

					//wp_editor( '', 'jwf_gallery_detail_content_'.$key, $settings );
					break;
				case "editor":
					$custom_field =  $term_meta;
					$control .= '<div id="wp-content-editor-container-'.$key.'" class="wp-editor-container  wp-core-ui wp-editor-wrap tmce-active"><textarea class="wp-editor-area jwf-editor-control" style="height: 300px" autocomplete="off" cols="40" name="term_meta_'.$key.'" id="term_meta_'.$key.'">'.$custom_field.'</textarea></div>';
					break;
				case "listbox":
				case "listboxtax":
				case "contentlink":
				case "contentlink_withview":
					if(!empty($listData)){
						$control .= '<select name="term_meta_'.$key.'" id="term_meta_'.$key.'" class="jejo-js-view-content-select">';
						$urlselect = "";
						foreach ($listData as $keyData => $valueData){
							$selected = ($keyData != sanitize_text_field($term_meta)) ? "" : ' selected="selected"';
							$url = home_url()."/wp-admin/post.php?post=".$keyData."&action=edit";
							$control .= '<option eurl="'.$url.'" value="'.$keyData.'" '.$selected.'>'.$valueData.'</option>';
							if(!empty($selected)) $urlselect = $url;
						}
						$control .= '</select>';
						if($type == "contentlink_withview" && (!empty($selected))){
							$control .= '&nbsp;&nbsp;<a class="jejo-js-view-content-link" href="'.$urlselect.'" target="_blank">View / Edit</a>';
						}
					}
					break;
				case 'checkboxlist':
				case 'checkboxtax':
						$checked = '';
						$custom_field =  sanitize_text_field($term_meta);
						$arrCheckValues = explode(',',$custom_field);
						//$this->fieldString .= '<img src="'.jwf_theme_url("qtranslate/flags/").$flags[$language].'"/ style="padding-right: 10px;"/><br/><br/><div id="'.$slug.'-all" class="wp-tab-panel">';
						if(!empty($listData)){
							$control .= '<div id="'.$key.'-all" class="wp-tab-panel"><ul class="jwf-checklistbox-control-main categorychecklist form-no-clear">';
							foreach ($listData as $id => $title){
								$checked = '';
								if(in_array($id,$arrCheckValues)){
									$checked = " checked";
								}
								$control .= '<li><label for="term_meta_'.$key."_".$id.'"><input class="jwf-checklistbox-control" rel="term_meta_'.$key.'"  checkboxlistclass-term_meta_'.$key.'" '.$checked.' type="checkbox" id="term_meta_'.$key."_".$id.'" value="'.$id.'"/>'.$title.'</label></li>';
							}
							$control .= '</ul>';
							$control .= '<p><input type="hidden" name="term_meta_'.$key.'" id="term_meta_'.$key.'" value="'.$custom_field.'"/></p>';

						$control .= '</div>';

						}else{
							$control .= '<div id="'.$slug.'-all" class="wp-tab-panel"><br/>No Data<br/></div>';
						}
						//echo  $control;
						break;
			}
			if(!empty($field[2])){ // note
					$control .=  '<p class="description">'.$field[2].'</p>';
			}
			$control .=  '</td></tr>';
			if(!empty($this->tabs) && !empty($field_tab_id)){
				if(empty($htmlArray[$field_tab_id])){
					$tabcode = sanitize_title($this->tabs[$field_tab_id])."-".$field_tab_id;
					$htmlArray[$field_tab_id] = '<div id="'.$tabcode.'" class="jwf-tab-content">
				<table class="form-table">'.$control;
				}else{
					$htmlArray[$field_tab_id] .= $control;
				}
			}else{
				$htmlText .= $control;
			}
			$control = "";
		}
		if(!empty($htmlText)){
			echo $htmlText;
		}
		if(!empty($htmlArray)){
			echo $tabsHTML;
			foreach($htmlArray as $tabK => $tabV){
				$htmlArray[$tabK] .="</table></div>";
				echo $htmlArray[$tabK];
			}
			echo '</div></div></td></tr>';
		}

		//wp_enqueue_media();
	}

	public function saveFields( $term_id ) {
		$code = $this->code;
		if(empty($this->code)) $code = $this->slug;
		$t_id = $term_id;
		$fields = $this->fieldList;
		foreach($fields as $field){
			$key = $field[0]; // key
			$type = $field[3]; // type
			$term_meta = get_term_meta($t_id, $key, true);
			if ( isset ( $_POST['term_meta_'.$key]) ) {
				$value= $_POST['term_meta_'.$key];
				switch ($type) {
					case "gallery";
						if(is_array($value)){
							//$IDs = implode(",",$value["id"]);
							if ( isset($value["id"]) ) {
								$jsonGallery = json_encode($value,JSON_UNESCAPED_UNICODE);
								update_term_meta($t_id,$key."_detail",$jsonGallery);
							}
							//update_term_meta($t_id,$key,$IDs);
						}
						break;
					case "image":
						$domain  = home_url();
						$value = str_replace($domain,"",$value);
						update_term_meta($t_id,$key,$value);
						if ( isset ( $_POST['term_meta_'.$key."_id"]) ) {
							$value= $_POST['term_meta_'.$key."_id"];
						}
						update_term_meta($t_id,$key."_id",$value);
						break;
					case "contentlink":
							update_term_meta($t_id,$key,$value);
							$editLink = esc_url( get_edit_term_link( $t_id, $code, $this->post_type ) );
							$contentInfo = '<p>This page has been linked to a�taxonomy. Please go to the page�below�for update content :��</p><p><a href="'.$editLink.'">'.$editLink.'</a></p>';
							if(empty($value)) $contentInfo = "";
							$my_post = array(
							  'ID'           => $value,
							  'post_content' =>  $contentInfo,
							);
							$post_id = wp_update_post( $my_post,true );
						break;
					default :
						update_term_meta($t_id,$key,$value);
						break;
				}
			}
		}
	}

	public function insertFields( $term_id ) {
		$t_id = $term_id;
		$fields = $this->fieldList;
		foreach($fields as $field){
			$key = $field[0]; // key
			$term_meta = get_term_meta($t_id, $key, true);
			if ( isset ( $_POST['term_meta_'.$key]) ) {
				$value= $_POST['term_meta_'.$key];
				if(is_array($value)){
					$value = implode(",",$value);
				}
				add_term_meta($t_id,$key,$value);
			}
		}
	}

	static public function getTermListBreadcrumb($term,$taxonomy,$htmlItems = array('<','>')) {

	        $term = get_term_by('id',$term, $taxonomy );
//print_r($term);
	        $tmpTerm = $term;
	        $tmpCrumbs = array();
	        while ($tmpTerm->parent > 0){
	            $tmpTerm = get_term($tmpTerm->parent, $taxonomy);
	            $crumb = $htmlItems[0].'<a href="' . get_term_link($tmpTerm, $taxonomy) . '">' . $tmpTerm->name . '</a>'.$htmlItems[1];
	            array_push($tmpCrumbs, $crumb);
	        }
	        return implode('', array_reverse($tmpCrumbs));

}

}
?>
