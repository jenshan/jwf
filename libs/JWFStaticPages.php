<?php
class JWFStaticPages {
	private $pages = array();

	function __construct() {
		//$this->init();
    }
    public function run() {
			if(is_multisite()){
				$JWFCommons = new JWFCommons();
				//$currentSiteID = get_current_blog_id();
				$siteIDs = $JWFCommons->getBlogIDList();
				foreach($siteIDs as $siteID){
						switch_to_blog( $siteID );
						$this->process();
						restore_current_blog();
				}
			}else{
				$this->process();
			}
    }
	public function process() {
		// do anything for init
		add_action( 'init', array($this,'setSlug' ));
		/* Handle template redirect according the template being queried. */
		add_action( 'template_redirect', array($this,'setTemplate') );
	}

	public function set($slug,$file){
		$obj = $this->pages;
		$obj[$slug] = $file;
		$this->pages = $obj;
	}

	function setSlug() {
		global $wp, $wp_rewrite;
	   // $wp->add_query_var( 'api' );
		if(!empty($this->pages)){
			foreach ($this->pages as $slug => $page){
				add_rewrite_endpoint( $slug, EP_ROOT );
			}
			$wp_rewrite->flush_rules();
		}
		//print_r($wp_rewrite); exit;
	}

	function setTemplate() {
		global $wp;
		$template = $wp->query_vars;
		if(!empty($this->pages)){
			foreach ($this->pages as $slug => $page){
				if ( array_key_exists( $slug, $template ) ) {
					/* Make sure to set the 404 flag to false, and redirect
						   to the assignment-api page template. */
					global $wp_query;
					$wp_query->set( 'is_404', false );
					$wp_query->is_home = false;
					include_once $page;
					exit;
				}
			}
		}
    }




}
?>
