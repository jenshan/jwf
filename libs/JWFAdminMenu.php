<?php
class JWFAdminMenu {
	private $objLabel = array();
	function __construct() {
		//$this->init();
    }
    public function init() {
		// do anything for init
    }

		/* ==============================
		Available Values for updating Label

		$labels["name"] = 'News';
		$labels["singular_name"] = 'News';
		$labels["add_new"] = 'Add News';
		$labels["add_new_item"] = 'Add News';
		$labels["edit_item"] = 'Edit News';
		$labels["new_item"] = 'New News';
		$labels["view_item"] = 'View News';
		$labels["view_items"] = 'View News';
		$labels["search_items"] = 'Search News';
		$labels["not_found"] = 'No news found.';
		$labels["not_found_in_trash"] = 'No news found in Trash.';
		$labels["parent_item_colon"] = 'Parent news'; // Not for "post"
		$labels["archives"] = 'News Archives';
		$labels["attributes"] = 'News Attributes';
		$labels["insert_into_item"] = 'Insert into news';
		$labels["uploaded_to_this_item"] = 'Uploaded to this news';
		$labels["featured_image"] = 'Featured Image';
		$labels["set_featured_image"] = 'Set featured image';
		$labels["remove_featured_image"] = 'Remove featured image';
		$labels["use_featured_image"] = 'Use as featured image';
		$labels["filter_items_list"] = 'Filter news list';
		$labels["items_list_navigation"] = 'News list navigation';
		$labels["items_list"] = 'News list';

		# Menu
		$labels["menu_name"] = 'News';
		$labels["all_items"] = 'All News';
		$labels["name_admin_bar"] = 'News';

		*****************************/

		public function setMenuLabel($contentType, $listLabel){
			if(!empty($listLabel) && !empty($listLabel) && is_array($listLabel)){
					$this->objLabel = $listLabel;
					add_filter( 'post_type_labels_'.$contentType, array($this,'renameMenuLabels'));
					return true;
			}
			return false;
		}

	/**
	* Rename Labels on Admin Menu
	*
	* @param object $labels
	* @hooked post_type_labels_post
	* @return object $labels
	*/
 	function renameMenuLabels( $labels )
	{
			$newLabels = $this->objLabel;
			# Labels
			foreach($newLabels as $key => $value){
				if(!empty($value)){
					$labels->{$key} = $value;
				}
			}

	    return $labels;
	}
}
?>
