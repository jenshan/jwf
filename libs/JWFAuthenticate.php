<?php
class JWFAuthenticate {

	public function __construct() {
		//$this->init();
	}

	public function init() {
		// do anything for init
	}
	public function autoLogin($username,$urlAfterLogin = "") {
		$user = get_user_by('login', $username ); // Uses existing WordPress function "get_user_by"

		if ( !is_wp_error( $user ) )
		{
			//uses WordPress global variable $current_user;
			wp_get_current_user();
				if ( user_can( $user, "subscriber" ) ){   //limit to subscriber level only
				   wp_clear_auth_cookie();  //clear the cookie if one exists
				   wp_set_current_user ( $user->ID ); //set id to what we want it to be
				   wp_set_auth_cookie  ( $user->ID ); //set the cookie
				   if(empty($urlAfterLogin)) $urlAfterLogin = home_url();  //redirect URL to home page.  *Change this if desired.
				   wp_safe_redirect( $urlAfterLogin );
				   exit();
		  } else {
			  wp_clear_auth_cookie();  //If user doesn't exist, clears the cookie and redirects to home.
			  if(empty($urlAfterLogin)) $urlAfterLogin = home_url();
			  wp_safe_redirect( $urlAfterLogin );
			  exit();
		  }
		}
	}
}
?>
