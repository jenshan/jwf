<?php
class JWFContent {

    function __construct() {
        //$this->init();
    }

    public function init() {

	}

	static public function getListByContentType($type,$w='',$h='',$limit=-1,$argsMore = "",$argsPaging = null){
		global $paged;
		if(isset($_REQUEST["paged"])){
			 $paged = $_REQUEST["paged"];
		}
    if(!empty($argsMore["paged"])){
      $paged = $argsMore["paged"];
    }
		if(empty($paged)){
			$paged = 1;
		}
		$args = array('paged'=>$paged,
					  'posts_per_page'   => $limit,
					//	'order' => 'post_date DESC',
						'post_type'        => $type,
						'post_status'      => 'publish');
		if(!empty($argsMore))
			$args =	array_merge($args, $argsMore);

		$recent = new WP_Query($args);

		$range=4;
		$page_max=0;
		$pagination=self::getPagination($range , $recent, $page_max, $argsPaging);

		$returnContent=array();
    $totalPage = $recent->max_num_pages;
    $totalRecord = $recent->found_posts;
    $postCount = $recent->post_count;
		while($recent->have_posts())
		{
			$recent->the_post();
			$id = get_the_id();
			$title = get_the_title();
			$permalink=get_permalink();
			$excerpt = get_the_excerpt();
			$content = get_the_content();
			$thumbnail = JWFAttachment::getThumbnail($id,$w,$h,true);
			$returnContent[$id]=array('image'=>$thumbnail,
									'title'=>$title,
									'permalink'=>$permalink,
									'excerpt'=>$excerpt,
									'content' => $content);
		}

		wp_reset_query();

		$results['ListData']=$returnContent;
		$results['Pagination']=$pagination;
    $results['Variable']['totalPage'] = $totalPage;
    $results['Variable']['totalRecord'] = $totalRecord;
    $results['Variable']['postCount'] = $postCount;
		return $results;
	}
	static public function getPagination($range = 8, $wp_query, $max_page = 0,$settings = null){
		// $paged - number of the current page
		global $paged;
    if (empty($settings)){
      $settings["page"] = "Page";
      $settings["prev"] = "Previous";
      $settings["next"] = "Next";
    }
		$html="";
		if(empty($wp_query))  global $wp_query;

		if(isset($_REQUEST["paged"])){
			 $paged = $_REQUEST["paged"];
		}

	  // How much pages do we have?
	  if ( empty($max_page) ) {
		$max_page = $wp_query->max_num_pages;
	  }
	  // We need the pagination only if there are more than 1 page
	  if($max_page > 1){
		if(!$paged){
		  $paged = 1;
		}
		$html.= '<ul class="pagination">';

	   if($paged <= $max_page && $paged != 1){
				$html.= '<li class="pagi-item"><a rel="prev" pdata="'.($paged-1).'" class="link-pagination" href="'.get_pagenum_link($paged-1).'" title="'.$settings['page'].' '.($paged-1).'">'.$settings['prev'].'</a></li>';
		}else{
			$html.= '<li class="pagi-item disable"><a rel="prev" class="link-pagination" href="#">'.$settings['prev'].'</a></li>';
		}
		// To the previous page
		// We need the sliding effect only if there are more pages than is the sliding range
		if($max_page > $range){
		  // When closer to the beginning
		  if($paged < $range){
			for($i = 1; $i <= ($range + 1); $i++){
				if($i==$paged) {
					$html.= '<li class="pagi-item active"><a pdata="'.$i.'" class="link-pagination" href="' . get_pagenum_link($i) .'" title="'.$settings['page'].' '.$i.'">'.$i.'</a></li>';
				}
				else {
					$html.= '<li class="pagi-item"><a pdata="'.$i.'" class="link-pagination" href="' . get_pagenum_link($i) .'" title="'.$settings['page'].' '.$i.'">'.$i.'</a></li>';
				}
			}
		  }
		  // When closer to the end
		  elseif($paged >= ($max_page - ceil(($range/2)))){
			for($i = $max_page - $range; $i <= $max_page; $i++){
				if($i==$paged) {
					$html.= '<li class="pagi-item active"><a pdata="'.$i.'" class="link-pagination" href="' . get_pagenum_link($i) .'" title="'.$settings['page'].' '.$i.'">'.$i.'</a></li>';
				}
				else {
					$html.= '<li class="pagi-item"><a pdata="'.$i.'" class="link-pagination" href="' . get_pagenum_link($i) .'" title="'.$settings['page'].' '.$i.'">'.$i.'</a></li>';
				}
			}
		  }
		  // Somewhere in the middle
		  elseif($paged >= $range && $paged < ($max_page - ceil(($range/2)))){
			for($i = ($paged - ceil($range/2)); $i <= ($paged + ceil(($range/2))); $i++){
				if($i==$paged) {
					$html.= '<li class="pagi-item active"><a pdata="'.$i.'" class="link-pagination" href="' . get_pagenum_link($i) .'" title="'.$settings['page'].' '.$i.'">'.$i.'</a></li>';
				}
				else {
					$html.= '<li class="pagi-item"><a pdata="'.$i.'" class="link-pagination" href="' . get_pagenum_link($i) .'" title="'.$settings['page'].' '.$i.'">'.$i.'</a></li>';
				}
			}
		  }
		}
		// Less pages than the range, no sliding effect needed
		else{
		  for($i = 1; $i <= $max_page; $i++){
				if($i==$paged) {
					$html.= '<li class="pagi-item active"><a pdata="'.$i.'" class="link-pagination" href="' . get_pagenum_link($i) .'" title="'.$settings['page'].' '.$i.'">'.$i.'</a></li>';
				}
				else {
					$html.= '<li class="pagi-item"><a pdata="'.$i.'" class="link-pagination" href="' . get_pagenum_link($i) .'" title="'.$settings['page'].' '.$i.'">'.$i.'</a></li>';
				}
		  }
		}
	  //  echo '</div><div class="te_left_right">';
		// On the first page, don't put the First page link


		if($paged >= 1 && $paged != $max_page){
			$html.= '<li class="pagi-item"><a pdata="'.($paged+1).'" rel="next" class="link-pagination" href="' . get_pagenum_link($paged+1) . '" title="'.$settings['page'].' '.($paged+1).'">'.$settings['next'].'</a></li>';
		}else{
			$html.= '<li class="pagi-item disable"><a rel="next" class="link-pagination" href="#">'.$settings['next'].'</a></li>';
		}




		$html.= '</ul>';

	  }

	  return $html;
	}

	static public function get_the_exceprt_id($post_id){
		$text = apply_filters('the_excerpt', get_post_field('post_excerpt', $post_id));
		return $text;
	}

	static public function get_the_content_id($post_id){
		$text = apply_filters('the_content', get_post_field('post_content', $post_id));
		return $text;
	}
	static public function getContentList($type, $amount = -1){
		$args = array('order' => 'ASC', 'posts_per_page' => $amount,'post_type'=>$type,'post_status' => 'publish');
		$recent = new WP_Query($args);
		$content='';
		if($recent->have_posts())
		{
			while($recent->have_posts())
			{
				$recent->the_post();
				$id = get_the_id();
				$title = get_the_title();
				$permalink=get_permalink();
				$thumbnail = JWFAttachment::getThumbnail($id,'193','290',true);
				$content[$id]=array(
									'title'=>$title,
									'thumbnail'=>$thumbnail,
									'permalink'=>$permalink
									);
			}
		}
		wp_reset_query();
		return $content;
    }
    static public function getContentListForDropdownList($type, $amount = -1,$argMore=array()){
      $args = array('order' => 'ASC', 'posts_per_page' => $amount,'post_type'=>$type,'post_status' => 'publish');
      if(!empty($argMore)){
        $args =	array_merge($args, $argMore);
      }
    //  var_dump($args);
      $recent = new WP_Query($args);
      $content= array();
      if($recent->have_posts())
      {
        while($recent->have_posts())
        {
          $recent->the_post();
          $id = get_the_id();
          $title = get_the_title();
          $permalink=get_permalink();
          $content[$id]=$title;
        }
      }
      wp_reset_query();
      return $content;
      }



	static public function getContentByName($name,$type){
		$args = array('order' => 'ASC', 'posts_per_page' => 1,'post_type'=>$type,'post_status' => 'publish', 'name' => $name);
		$recent = new WP_Query($args);
		$content='';
		if($recent->have_posts())
		{
			$recent->the_post();
			$content["id"] = get_the_id();
			$content["excerpt"] = get_the_excerpt();
		}
		wp_reset_query();
		return $content;
    }

	static public function getMenuItems($menu_name){
		$menuObj = array();
		if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
			$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );

			$menu_items = wp_get_nav_menu_items($menu->term_id);

			//print_r($menu_items);exit;
			$i = 0;
			$menuObj["name"] = $menu->name;
			$menuObj["slug"] = $menu->slug;
			$menuObj["list"] =array();
			foreach ( (array) $menu_items as $key => $menu_item ) {
				$menuObj["list"][$i]["title"] = $menu_item->title;
				$menuObj["list"][$i]["url"] = $menu_item->url;
				$menuObj["list"][$i]["postid"] = $menu_item->object_id;
				$menuObj["list"][$i]["description"] = $menu_item->description;
				$menuObj["list"][$i]["id"] = $menu_item->ID;
				$menuObj["list"][$i]["target"] = $menu_item->target;
        $menuObj["list"][$i]["classes"] = $menu_item->classes;
        if(!empty($menu_item->classes) && is_array($menu_item->classes)){
				  $menuObj["list"][$i]["classes"] = implode(" ",$menu_item->classes);
        }
				$i++;
			}

		}
		return $menuObj;
	}

	static public function getMenuHTML($menu_name,$menu_class = "",$active_class="",$menu_id="",$iconHTML="",$display = false,$menu_sub_class = "",$menu_has_sub_class="",$itemWrap = ""){
		$args = array( 'echo' => false,
						'container' => '',
						'container_class' => '',
						'menu_class' => $menu_class,
						'menu_id'    => $menu_id,
						'theme_location' => $menu_name,
						'link_before' => $iconHTML);
		if(!empty($itemWrap)){
			$args['items_wrap'] = $itemWrap; //'<ul>%3$s</ul>';
		}
		$menu = wp_nav_menu( $args );
		$menu = preg_replace("/(current-menu-item)|(current-post-ancestor)|(current-menu-ancestor)|(current-menu-parent)|(current-page-parent)|(current-page-ancestor)/", $active_class, $menu); // replace
		/*if ( is_front_page() ) {
			$menu = preg_replace("/(menu-home)/", $active_class, $menu); // replace
		} */
		if(!empty($menu_sub_class)){
			$menu = preg_replace("/(sub-menu)/", $menu_sub_class, $menu); // replace
		}
		if(!empty($menu_sub_class)){
			$menu = preg_replace("/(menu-item-has-children)/", $menu_has_sub_class, $menu); // replace
		}
		if($display)
			echo $menu;

    return $menu;

  }
      static public function getMenuHTMLbyMenuID($termID,$menu_class = "",$active_class="",$menu_id="",$iconHTML="",$display = false,$menu_sub_class = "",$menu_has_sub_class="",$itemWrap = ""){
    		$args = array( 'echo' => false,
                'menu'=>  $termID,
    						'container' => '',
    						'container_class' => '',
    						'menu_class' => $menu_class,
    						'menu_id'    => $menu_id,
    		//				'theme_location' => $menu_name,
    						'link_before' => $iconHTML);
    		if(!empty($itemWrap)){
    			$args['items_wrap'] = $itemWrap; //'<ul>%3$s</ul>';
    		}
    		$menu = wp_nav_menu( $args );
    		$menu = preg_replace("/(current-menu-item)|(current-post-ancestor)|(current-menu-ancestor)|(current-menu-parent)|(current-page-parent)|(current-page-ancestor)/", $active_class, $menu); // replace
    		/*if ( is_front_page() ) {
    			$menu = preg_replace("/(menu-home)/", $active_class, $menu); // replace
    		} */
    		if(!empty($menu_sub_class)){
    			$menu = preg_replace("/(sub-menu)/", $menu_sub_class, $menu); // replace
    		}
    		if(!empty($menu_sub_class)){
    			$menu = preg_replace("/(menu-item-has-children)/", $menu_has_sub_class, $menu); // replace
    		}
    		if($display)
    			echo $menu;

		return $menu;
	}

  static public function getMenuName( $theme_location ) {
  	if( ! $theme_location ) return false;

  	$theme_locations = get_nav_menu_locations();
  	if( ! isset( $theme_locations[$theme_location] ) ) return false;

  	$menu_obj = get_term( $theme_locations[$theme_location], 'nav_menu' );
  	if( ! $menu_obj ) $menu_obj = false;
  	if( ! isset( $menu_obj->name ) ) return false;

  	return $menu_obj->name;
  }


	static public function get_meta_value_by_meta_key( $meta_key ) {
		global $wpdb;
		$output = array();
		$results = $wpdb->get_results( $wpdb->prepare("SELECT DISTINCT meta_value FROM $wpdb->postmeta WHERE meta_key = %s " , $meta_key), ARRAY_A );
		if( $results ) {
			foreach( $results as $res ) {
				if( !empty( $res['meta_value'] ) ) {
					$output[] = $res['meta_value'];
				}
			}
		}
		return $output;
	}

	static public function getImageFromSetting($key) {
		$image_id = get_option($key);
    $image_url = "";
		if(!empty($image_id)){
			$image_url = wp_get_attachment_url($image_id);
		}else{
      if(is_multisite()){
        $blog_id = get_current_blog_id();

        if(!is_main_site($blog_id)){
          switch_to_blog(1);
          $image_id = get_option($key);
          $image_url = wp_get_attachment_url($image_id);
          restore_current_blog();
        }
      }
    }
		return $image_url;
	}

  static public function loadImgFromPrimaryBlog($primaryBlogID, $image_id, $width = "", $height = ""){
    $imgObj = array();
    if(is_multisite()){
      $blog_id = get_current_blog_id();

    //  if(!is_main_site($blog_id)){
      if(!empty($primaryBlogID) && $primaryBlogID != $blog_id){
        switch_to_blog($primaryBlogID);
        $imgObj['id'] = $image_id;
        if(!empty($width) || !empty($height)){
          $imgObj['thumb'] = JWFAttachment::getThumbnailByImgID($image_id,$width,$height, true);
        }else{
            $imgObj['thumb'] = JWFAttachment::getThumbnailByImgID($image_id,'','', false);
        }
        restore_current_blog();
      }
    }
    return $imgObj;
  }
	static public function getGalleryField($contentID,$meta_key, $width='', $height='' ) {

  /*  if(is_multisite()){
      $blogID = get_current_blog_id();
      $currentBlogID = $blogID;
      $sitePostID = get_post_meta($contentID,'_jwf_siteid_'.$meta_key,true);
      $JWFThreeBroadCast = new JWFThreeBroadCast();
      $postIDs = $JWFThreeBroadCast->getPostDataFromBlogs($contentID,$currentBlogID);
    //  $isBlogChild=$JWFThreeBroadCast->isBlogChild($blogID,$contentID);
  //		$data = $JWFThreeBroadCast->getDataLinkedPostFromBlogID($blogID,$contentID);
      if(!empty($sitePostID) && !empty($currentBlogID) && $currentBlogID != $sitePostID ){
        if(isset( $postIDs) && !empty( $postIDs[$sitePostID])){
          $is_switch_blog = TRUE;
          switch_to_blog( $sitePostID );
          $contentID = $postIDs[$sitePostID];
          //$custom_field = get_post_meta($contentID,$slug,true);
        }
      }
    }*/
		$gallery = get_post_meta($contentID, $meta_key."_detail",true);
    $galleryBlog = get_post_meta($contentID, $meta_key."_site",true);

		$galleries = array();
		$galleriesID = array();
		$galleriesContent = array();
		if(empty($gallery)){
			$gallery = get_post_meta($contentID,$meta_key,true);
			$galleriesID = explode(",",$gallery);
		}else{
			$galleryJson = $gallery;
			$galleriesID = $galleryJson["id"];
			$galleriesContent = $galleryJson["content"];
			$galleriesLabel = $galleryJson["label"];
      $galleriesLink = $galleryJson["link"];
      $galleriesImg = $galleryJson["img"];
    //  $galleriesThumb = $galleryJson["thumb"];

		}
    if(!empty($galleriesID)){
  		foreach($galleriesID as $key => $galleryID){
  			$galleries[$key]["id"] = "";
  			$galleries[$key]["content"] = "";
  			$galleries[$key]["label"] = "";
        $galleries[$key]["link"] = "";
        $galleries[$key]["img"] = "";
        $galleries[$key]["thumb"] = "";
        if(!empty($galleriesImg[$key])){
          $galleries[$key]["img"] = $galleriesImg[$key];
          $galleries[$key]["thumb"] =  JWFAttachment::getThumbnailByURL( $galleriesImg[$key],$width,$height, true);
        }
        if(!empty($galleryID)){
          if(!empty($galleriesImg[$key])){
            $galleryID =  JWFAttachment::getAttachmentIDbyValue($galleriesImg[$key]);
          }
          //$galleries[$key]["thumb"] =  JWFAttachment::getThumbURLbyPath( $galleryID,$width,$height, true);

          /*$gIDAttachment = "";
          if(!empty($galleriesImg[$key])){
            $gIDAttachment = JWFAttachment::getImageIDbyURL($galleriesImg[$key]);
            if(!empty($gIDAttachment) && $gIDAttachment != $galleryID){
              $galleryID = $gIDAttachment;
              $galleries[$key]["thumb"] =  JWFAttachment::getThumbnailByImgID( $galleryID,$width,$height, true);
            } else {
              $galleries[$key]["thumb"] =  JWFAttachment::getThumbnailByImgID( $galleryID,$width,$height, true);
            }
          }*/
          $galleries[$key]["id"] = $galleryID;
        }
  			if(!empty($galleriesContent[$key])){
  				$galleries[$key]["content"] = $galleriesContent[$key];
  			}
  			if(!empty($galleriesLabel[$key])){
  				$galleries[$key]["label"] = $galleriesLabel[$key];
  			}
        if(!empty($galleriesLink[$key])){
          $galleries[$key]["link"] = $galleriesLink[$key];
        }
  		}
    }

		return $galleries;
	}

  // use for WPMU
  static function checkFieldDataFromSites($siteMainID,$contentID,$keyArray = array()){
    $siteData = array();
    $JWFCommons = new JWFCommons();
    $siteIDs = $JWFCommons->getBlogIDList();
    if(empty($keyArray) || !is_array($keyArray)){ $keyArray = array(); }
    if(is_multisite() &&  !empty($keyArray)){
      foreach($keyArray as $key){
        $keyValueOrg[$key] = get_post_meta($contentID,$key,true);
      }
      foreach($siteIDs as $siteID){
        if($siteID != $siteMainID){
          switch_to_blog( $siteID );
            $isTheSame = "true";
            $listKeyNotTheSame = array();
            foreach($keyArray as $key){

              $keyValue = get_post_meta($contentID,$key,true);
              $listKeyNotTheSame[$key] = "true";
              if($keyValue != $keyValueOrg[$key]){
                $isTheSame = "false";
                $listKeyNotTheSame[$key] = "false";
              }
            }
            restore_current_blog();
            $siteData[$siteID]['results'] = $isTheSame;
            $siteData[$siteID]['keys'] = $listKeyNotTheSame;
        }
      }
      return $siteData;
    }
    else{
      return null;
    }
  }
static public function setTaxonomy($dataID, $nameObj, $taxonomyName, $symbolBreak = array(',','&'))
      {
          //$nameObj = JWFCommons::multi_explode($symbolBreak, $nameObj);
          //print_r($nameObj); echo "<br/>";
          $taxonomyId = array();
          foreach ($nameObj as $nameTax) {
              $nameTax = trim($nameTax);
              $taxonomyObj = get_term_by('name', $nameTax, $taxonomyName);
              //print_r($taxonomyObj);
              if ($taxonomyObj === false) {
                  $taxonomyArray = wp_insert_term($nameTax, $taxonomyName);
                  if (!is_wp_error($taxonomyArray)) {
                      $taxonomyId[] = (!empty($taxonomyArray['term_id'])) ? $taxonomyArray['term_id'] : 0;
                  }
              } else {
                  $taxonomyId[] = $taxonomyObj->term_id;
              }
          }
          if (!empty($taxonomyId)) {
              wp_set_object_terms($dataID, $taxonomyId, $taxonomyName);
          }
      }

/*
 * check the page ID is child page
 * @param $currentid current ID for checking. default is null and it will be get from $post global
 * @return	false for is not child | post ID of parent
 * @Author: Jenshan
 */
static public function has_parent_page($currentid = null){
  global $post;
  if(!empty($currentid)){
    $postGlobal = get_post($currentid);
  }else{
    if(empty($post)) return false;
    $postGlobal = $post;
  }

  $currentid = $postGlobal->ID;
  $hasParentPage = false;
  $idParentPage = $currentid;
  if ($postGlobal->post_parent != 0 && $postGlobal->post_parent != $currentid) {

      $parent = array_reverse(get_post_ancestors($postGlobal->ID));
      $idParentPage = (!empty($parent[0])) ? $parent[0] : $postGlobal->post_parent;
      //$idSubParent = $postGlobal->post_parent;
      $hasParentPage = $idParentPage;
      //$pageTemplate = get_page_template_slug($idParentPage);
  }
  return $hasParentPage;
}

/*
 * get list menus has been created in the menu section of admin.
 * @return	array $menuList with ID &  Name
 * @Author: Jenshan
 */

static public function getMenuList(){
  $menuList = array();
  $nav_menus = wp_get_nav_menus();
      if ( empty( $nav_menus ) || ! is_array( $nav_menus ) )
          return $menuList;
    //      var_dump($nav_menus	);

      foreach ( $nav_menus as $menu ) {
        $menuList[$menu->term_id] = $menu->name;

      }
  return $menuList;

}



}
