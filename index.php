<?php
/*
Plugin Name: Jenshan Wordpress Functions
Description: Provide useful functions for coding
Version: 1.0
Author: Jenshan
Author URI: http://www.jenshan.com
*/

include_once TEMPLATEPATH.'/models/jwf/libs/WPExtend/JEJO_WP_Image_Editor.php';
include_once TEMPLATEPATH.'/models/jwf/libs/WPExtend/JsonManifest.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFDateTime.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFCommons.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFThreeBroadCast.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFAdminMenu.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFTheme.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFJsonManifest.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFAttachment.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFAdminTables.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFLoginLogo.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFQtranslate.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFGoogle.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFStaticPages.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFContent.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFQrCode.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFCustomType.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFCustomTaxonomy.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFCustomField.php';
include_once TEMPLATEPATH.'/models/jwf/libs/JWFWebControls.php';
include_once TEMPLATEPATH.'/models/jwf/3rd/pdf/mpdf.php';
//include_once TEMPLATEPATH.'/models/jwf/libs/JWFAutoUpdate.php';

if (!class_exists('JWF_Plugin'))
{
	class JWF_Plugin
	{

		function __construct()
		{
			//register_activation_hook(__FILE__, array($this, 'activatePlugin'));
			//register_deactivation_hook(__FILE__, array($this, 'deactivatePlugin'));
			//register_uninstall_hook(__FILE__, array($this, 'uninstallPlugin'));

			$JWFCommons = new JWFCommons();
			$JWFCommons->init();

			//$JWFStaticPages = new JWFStaticPages();
			//$JWFStaticPages->set('timthumb',dirname( __FILE__ ).'/timthumb.php');
			//$JWFStaticPages->run();

			$JWFAttachment = new JWFAttachment();
			$JWFAttachment->init();

			$JWFContent = new JWFContent();
			$JWFContent->init();

			$JWFAdminTables = new JWFAdminTables();
			$JWFAdminTables->init();

			$JWFGoogle = new JWFGoogle();
			$JWFGoogle->init();
			$JWFQrCode = new JWFQrCode();
			$JWFQrCode->init();


			$JWFThreeBroadCast = new JWFThreeBroadCast();
			$JWFThreeBroadCast->init();
			if (is_admin() ) {
				add_action('admin_enqueue_scripts', array($this,'jwf_script_add'));
			}
			//add_action('init', array($this,'jwf_activate_au'));
		}

		public function jwf_activate_au()
	{
		// set auto-update params
		$plugin_current_version = '3.0.1';
		$plugin_remote_path     = 'https://bitbucket.org/jenshan/jwf/raw/master/update.txt';
		$plugin_slug            = TEMPLATEPATH.'/models/jwf/index.php';
		$license_user           = 'jenshan';
		$license_key            = '';

		// only perform Auto-Update call if a license_user and license_key is given
		if ( $license_user && $license_key && $plugin_remote_path )
		{
			new JWFAutoUpdate ($plugin_current_version, $plugin_remote_path, $plugin_slug, $license_user, $license_key);
		}
	}



		public function jwf_script_add( $hook ) {
		//	var_dump($hook);exit;
			if (('post.php' != $hook) && ('post-new.php' != $hook) && ('edit-tags.php' != $hook) && ('term.php' != $hook)) {
				return;
			}
			///wp-includes/js/tinymce/wp-tinymce.php?c=1&ver=4403-20160901
		//wp_enqueue_script( 'tiny_mce_js', get_option( 'siteurl' ) . '/wp-includes/js/tinymce/wp-tinymce.php?c=1&ver=4403-20160901');
		//wp_enqueue_script( 'tiny_mce_popup_js', get_option( 'siteurl' ) . '/wp-includes/js/tinymce/tiny_mce_popup.js');
		//	wp_enqueue_script( 'tinymce_js', includes_url( 'js/tinymce/' ) . 'tinymce.min.js', array( 'jquery' ), false, true );
		//	wp_register_style( 'tinymce_css', includes_url( 'css/' ). 'editor.min.css');
		//	wp_enqueue_style( 'tinymce_css' );
		if (!wp_doing_ajax()) {
		?><div style="display:none"><?php
		$settings = array(
    'textarea_name' => 'contentHidden',
    'media_buttons' => false,
		'wpautop' => false,
    'tinymce' => array(
        'theme_advanced_buttons1' => 'formatselect,|,bold,italic,underline,|,' .
            'bullist,blockquote,|,justifyleft,justifycenter' .
            ',justifyright,justifyfull,|,link,unlink,|' .
            ',spellchecker,wp_fullscreen,wp_adv',
						'quicktags' => array('buttons' => 'strong,em,link,block,del,ins,img,ul,ol,li,code,close')

    )
);
wp_editor( '', 'contentHidden', $settings );
		?></div>
		<script>
		var jwf_theme_url = "<?php echo get_template_directory_uri().'/models/jwf/';?>";
		</script>
		<?php
		}
			add_thickbox();
			wp_enqueue_media();
			wp_enqueue_script( 'html2canvas_js', get_template_directory_uri() . '/models/jwf/scripts/html2canvas.js');

			wp_register_style( 'datetime_css', get_template_directory_uri() . '/models/jwf/scripts/datetime/jquery.datetimepicker.min.css',false,'1.8.2');
			wp_enqueue_style( 'datetime_css' );
		wp_enqueue_script( 'datetime_js', get_template_directory_uri() . '/models/jwf/scripts/datetime/jquery.datetimepicker.full.min.js');

			wp_register_style( 'chosen_css', get_template_directory_uri() . '/models/jwf/scripts/chosen/chosen.min.css',false,'1.8.2');
			wp_enqueue_style( 'chosen_css' );
			wp_enqueue_script( 'chosen_js', get_template_directory_uri() . '/models/jwf/scripts/chosen/chosen.jquery.min.js');
		//	wp_enqueue_script( 'chosen_order_js', get_template_directory_uri() . '/models/jwf/scripts/chosen/ext/chosen.order.jquery.min.js');

			wp_register_style( 'chosen_image_css', get_template_directory_uri() . '/models/jwf/scripts/chosen/ext/chosen-photo/chosen-photo.css',false,'1.0.0');
			wp_enqueue_style( 'chosen_image_css' );
			wp_enqueue_script( 'chosen_image_js', get_template_directory_uri() . '/models/jwf/scripts/chosen/ext/chosen-photo/chosen-photo.jquery.js');

			wp_register_style( 'jwf_custom_field_css', get_template_directory_uri() . '/models/jwf/styles/jwf_custom_field.css',false,'1.0.0' );
			wp_enqueue_style( 'jwf_custom_field_css' );
			wp_enqueue_script( 'jwf_custom_field_js', get_template_directory_uri() . '/models/jwf/scripts/jwf_custom_field.js');

			//stinyMCE.init();
		}

	}
}
$JWF_Plugin = new JWF_Plugin();

function jwf_theme_url($file){
	$url = get_template_directory_uri().'/models/jwf/'.$file;
	return $url;
}
?>
