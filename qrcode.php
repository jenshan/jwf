<?php
if(empty($_GET["t"])){
	echo "missing t param";
	exit();
}
header('Content-Type: image/png');
//if( file_exists(dirname(__FILE__) . '/timthumb-config.php'))	require_once('qrcode-config.php');
$text = $_GET["t"];
include('3rd/phpqrcode/qrlib.php'); 
 
// outputs image directly into browser, as PNG stream 
QRcode::png($text, false , QR_ECLEVEL_L, 2);
exit();
?>