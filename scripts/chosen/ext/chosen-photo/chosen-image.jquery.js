/*
 * Chosen jQuery plugin to add an image to the dropdown items.
 */
(function($) {
    $.fn.chosenPhotoPreview = function(options) {
        return this.each(function() {
            var $select = $(this);
            var imgPhotoPreview  = {};

            // 1. Retrieve img-src from data attribute and build object of image sources for each list item.
            $select.find('option').filter(function(){
                return $(this).text();
            }).each(function(i) {
                imgPhotoPreview[i] = '<img src="'.$(this).attr('data-photo-src').'"/>';
            });

            // 2. Execute chosen plugin and get the newly created chosen container.
            $select.chosen(options);
            var $chosen_container = $select.next('.chosen-container');
            $chosen_container.append('<div class="jwf-chosen-photo"></div>');
            $chosen_photo = $chosen_container.find(".jwf-chosen-photo");

            // 3. Style lis with image sources.
            $chosen_container.on('click.chosen, mousedown.chosen, keyup.chosen', function(event){
                var $photos = "";
                  $chosen_photo.html("");
                $chosen_container.find('.chosen-results li').each(function() {
                    var imgIndex = $(this).attr('data-option-array-index');
                    $photos += imgPhotoPreview[imgIndex];
                });
                $chosen_photo.html($photos);
            });
        });
    };
})(jQuery);
