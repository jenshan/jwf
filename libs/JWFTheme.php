<?php
class JWFTheme {
	public $css = array();
	public $js = array();
	private $manifest;

    function __construct() {
        //$this->init();
    }

    public function init() {
		//
	}
    public function register() {
		add_action('wp_enqueue_scripts',array($this,'addQueue'), 0);
	}

	public function addQueue(){
		include_once(TEMPLATEPATH. "/configs/load-css-js.php"); /* add by Jenshan */
		self::addCSSQueue($this->css);
		self::addJSQueue($this->js);
	}

	public static function getAssetPath($filename,$folder_min_name = 'dist'){		
		$dist_path = get_template_directory_uri() .'/'.$folder_min_name.'/';
		$directory = dirname($filename) . '/';
		$file = basename($filename);
		static $manifest;

		if (empty($manifest)) {
			$manifest_path = get_template_directory() . '/'.$folder_min_name.'/'. 'assets.json';
			$manifest = new JsonManifest($manifest_path);
		}
		if (array_key_exists($file, $manifest->get())) {
			return $dist_path . $directory . $manifest->get()[$file];
		} else {
			return $dist_path . $directory . $file;
		}
	}

	/*********************************
	 The $args will be array format. sample:
		array(
			array("name","path","screen"),
			array("name","path","screen")
			);
	+ the "screen" will be: "all","(max-width: 63.93em)"
	*/
	public static function addCSSQueue($args){
		if(empty($args) || !is_array($args)) return false;
		foreach($args as $arg){
			if(!empty($arg) && is_array($arg)){
				$evalstr = "";
				$name = $arg[0];
				$path = $arg[1];
				$screen = $arg[2];
				wp_enqueue_style($name, $path,array(), false,$screen);
			}
		}
		return true;
	}
	/*********************************
	 The $args will be array format. sample:
		array(
			array("name","path","depend"),
			array("name","path","depend")
			);
	+ the "depend" will be javascript name for depending of current js with it: [jquery]
	*/
	public static function addJSQueue($args){
		if(empty($args) || !is_array($args)) return false;
		foreach($args as $arg){
			if(!empty($arg) && is_array($arg)){
				$evalstr = "";
				$name = $arg[0];
				$path = $arg[1];
				$depend = $arg[2];
				wp_enqueue_script($name, $path,$depend, null, true);
			}
		}
		return true;
	}


}
