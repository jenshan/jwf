<?php
class JWFQtranslate {	

	function __construct() {
//		$this->init();
    }
 
	function detectURL( $text ) {
        if ( preg_match_all('~(.*?)\|(\w{2,})\|~', $text, $matches) ) {
            $text = '';
			$textnl = "[:nl]";
			$textfr = "[:fr]";
			$texten = "[:en]";
            foreach ($matches[1] as $i => $match) {
                //$text .= "[:{$matches[2][$i]}]$match";
				if(empty($match)) $match = '#';
				switch ($matches[2][$i]){
					case 'nl':
						$textnl = "[:nl]$match";
						break;
					case 'fr':
						$textfr .= "[:fr]$match";
						break;
					default:
						$texten .= "[:en]$match";
						break;
				}
                
            }
			$text = $texten. $textfr. $textnl;
			$text = __( $text );			
        }
        return $text;
    }
	
    public function init() { 		
		add_filter('walker_nav_menu_start_el', array( $this,'qtransInNavEl'), 10, 4);			
		add_action('admin_init', array( $this,'qtranslate_edit_taxonomies'));	
    }	

	// Fix for qTranslate plugin and "Home" menu link reverting back to default language
	function qtransInNavEl($output, $item, $depth, $args) {
		global $GLOBAL_CURLANG;
		$url = esc_attr( $this->detectURL( $item->url ));
		if (function_exists('qtrans_convertURL')) {		
			$attributes = !empty($item->attr_title) ? ' title="' . esc_attr($item->attr_title) . '"' : '';
			$attributes .=!empty($item->target) ? ' target="' . esc_attr($item->target) . '"' : '';
			$attributes .=!empty($item->xfn) ? ' rel="' . esc_attr($item->xfn) . '"' : '';
			// Integration with qTranslate Plugin
			$attributes .=!empty($item->url) ? ' href="' . esc_attr( qtrans_convertURL($url,$GLOBAL_CURLANG) ) . '"' : '';
			$output = $args->before;
			$output .= '<a' . $attributes . '>';
			$output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
			$output .= '</a>';
			$output .= $args->after;
		}
		return $output;
	}
	
	// fix for qTranslate plugin about multi language of taxonomy
	function qtranslate_edit_taxonomies(){
	   $args=array(
		  'public' => true ,
		  '_builtin' => false
	   );
	   $output = 'object'; // or objects
	   $operator = 'and'; // 'and' or 'or'

	   $taxonomies = get_taxonomies($args,$output,$operator); 

	   if  ($taxonomies) {
		 foreach ($taxonomies  as $taxonomy ) {
			 add_action( $taxonomy->name.'_add_form', 'qtrans_modifyTermFormFor');
			 add_action( $taxonomy->name.'_edit_form', 'qtrans_modifyTermFormFor');        

		 }
	   }

	}
	public function qtranslate_set_default_actived_lang($q_config){		
		if(QTRANSLATE_DISABLE_CONFIG){
			$q_config['enabled_languages'] = array(
				'0' => 'nl',
				'1' => 'fr', 
				'2' => 'en'
			);
			$q_config['default_language'] = 'en';
			$q_config['detect_browser_language'] = false;
		}
		return $q_config;
	}
	
}
?>