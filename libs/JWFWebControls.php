<?php
class JWFWebControls {

    function __construct() {
        //$this->init();
    }

    public function init() {

	}

	static public function showTermDropdown($taxonomy, $name, $class, $activeSlug="", $showDefaultItem= "", $showParentOf="",$hideEmpty = true, $echo = true,$showNumberItems = true){

		$args = array(
				'hide_empty' => $hideEmpty
			);
		if($showParentOf !== ""){
			$args["parent"] = $showParentOf;
		}
		$terms = get_terms( $taxonomy, $args );
		$output ='<select class="'.$class.'" name="'.$name.'" id="'.$name.'">';

		$dataItems = array();
		foreach($terms as $term){
			$term_taxonomy=$term->taxonomy;
			$term_slug=$term->slug;
			$term_id=$term->term_id;
			$isVisible = get_term_meta($term_id , "slz_property_status_visible", true);
			//var_dump($isVisible);
			if($isVisible === 0 || $isVisible === "0") continue;
			$term_name = $term->name;
			$name2 = get_term_meta($term_id , "slz_property_status_title_2", true);

			if(!empty($name2))
				$term_name = $name2;
			$dataItems[$term_slug]	= $term_name;
		}

		if(!empty($showDefaultItem)){
			if(is_array($showDefaultItem)){
				$showDefaultItem = $showDefaultItem + $dataItems;
			}else{
				$output .='<option value="">'.$showDefaultItem.'</option>';
				$showDefaultItem = $dataItems;
			}
			foreach($showDefaultItem as $keyv => $keyn){
				$selected = ' ';
				if($keyv == $activeSlug){
					$selected = ' selected="selected"';
				}
				$output .='<option value="'.$keyv.'"'.$selected.'>'.$keyn.'</option>';
			}
		}

		$output .="</select>";
		if($echo){
			echo $output;
		}
		return $output;
	}

	static public function getDataForListbox($contentType){
		$args = array(
			'post_type' => $contentType,
			'showposts' => -1,
			'post_status' => 'publish',
			'orderby' => array( 'title' => 'ASC')
		);
		$pageData = array();
		wp_reset_postdata();
		$objQueryMain = new WP_Query($args);
		if ($objQueryMain->have_posts()) {
			$pageData[0]= " ----- Select -----";
			while ($objQueryMain->have_posts()) {
				$objQueryMain->the_post();
				$pageId = get_the_ID();
				$pageTitle = get_the_title();
				$pageData[$pageId]= $pageTitle;
			}
		}
		return $pageData;
		wp_reset_query();
		wp_reset_postdata();
	}


	static public function getTermForCheckbox($taxonomy){
		$termData = array();
		$args = array(
				'taxonomy' => $taxonomy,
				'hide_empty' => false
		);
		$terms = get_terms($args );
		//var_dump( $taxonomy);
		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
			foreach($terms as $term){
				$term_name = $term->name;
				$term_id=$term->term_id;
				$termData[$term_id]= $term_name;
			}
		}
		return $termData;
	}

  static public function getTermForCheckboxAdmin($taxonomy,$additionalTermMetaKey=array(), $symbol=' '){
		$termData = array();
		$args = array(
				'taxonomy' => $taxonomy,
				'hide_empty' => false
		);
    $term_query = new WP_Term_Query();
    $terms = $term_query->query( $args );

		if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
			foreach($terms as $term){
				$term_name = $term->name;
        $term_id=$term->term_id;
        if(!empty($additionalTermMetaKey)){
          $valueAdd = array($term_name);
          foreach($additionalTermMetaKey as $key){
              $valueAdd[] = get_term_meta($term_id, $key, true);
          }
          $term_name = implode($symbol,$valueAdd);
        }

				$termData[$term_id]= $term_name;
			}
		}
		return $termData;
	}

}
