<?php
class JWFDateTime {

    function __construct() {
        //$this->init();
    }

    public function init() {

	}

	static public function checkDurationAvailable($start, $end,  $today = "now"){
    $nt_submit_today = strtotime($today);
    $available = true;
    if(!empty($start) && !empty($end)){
      $nt_submit_start = (!empty($start)) ? strtotime($start) : $nt_submit_today + 1 ;
      $nt_submit_end = (!empty($end)) ? strtotime($end) : 0;
      $available = false;
      if(($nt_submit_today >= $nt_submit_start) && ($nt_submit_end >= $nt_submit_today)){
        $available = true;
      }
    }
		return $available;
	}

    static public function timeAgo($timestamp,$subtext){
      $datetime1 = new DateTime("now");
      $datetime2 = new DateTime();
      $datetime2->setTimestamp($timestamp);
      $diff=date_diff($datetime1, $datetime2);
      $timemsg='';
      if($diff->y > 0){
          $timemsg = $diff->y .' year'. ($diff->y > 1?"s":'');

      }
      else if($diff->m > 0){
       $timemsg = $diff->m . ' month'. ($diff->m > 1?"s":'');
      }
      else if($diff->d > 0){
       $timemsg = $diff->d .' day'. ($diff->d > 1?"s":'');
      }
      else if($diff->h > 0){
       $timemsg = $diff->h .' hr'.($diff->h > 1 ? "s":'');
      }
      else if($diff->i > 0){
       $timemsg = $diff->i .' min'. ($diff->i > 1?"s":'');
      }
      else if($diff->s > 0){
       $timemsg = $diff->s .' sec'. ($diff->s > 1?"s":'');
      }

  $timemsg = $timemsg.$subtext;
  return $timemsg;
  }
}
