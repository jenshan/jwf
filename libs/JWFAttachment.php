<?php
class JWFAttachment {
  static public $removeCurrentDomain = false;
	static public $suFormat = "--pitalent-thumb";
	function __construct() {
		/* $this->init();*/
    }

    public function init() {
      add_filter('upload_mimes',array( $this, 'add_custom_mime_types'));
    }

    public function set_post_attachment_featured_image(){
      add_filter('save_post',array( $this, '_hook_save_post_set_attachment_for_thumb'), 10, 3);
    }

    public function _hook_save_post_set_attachment_for_thumb($post_id, $post, $update ){
      $post_thumbnail_id = get_post_thumbnail_id($post_id );
   	 //print_r($post_thumbnail_id);exit;
   	 if(!empty($post_thumbnail_id)){
   		 self::setAttachmentForFeaturedImageOfPost($post->ID,$post_thumbnail_id);
   	 }


    }

    /*
     * Convert file path / url to URL with full domain
     * Limitation: Only use for WP and the upload path is not change the name , should be followed  on datetime format for name of WP
     * @param $file_path_url string The path / url of file with full
     * @param $subfix_name string The text will be added before extension of file
     * @param $is_check_exist bool Check the existing of file.
     * @return	$file_path_url string The url of image has been convert
     * @Author: Jenshan
     */

    static public function convert_file_path_to_URL($file_path_url,$subfix_name = "",$w="",$h="", $returnType = "url" ){


      $fileName = basename($file_path_url);
      $fileNameNoExtension =substr($fileName, 0, strrpos($fileName, "."));
      $fileExtension = str_replace($fileNameNoExtension.".","",$fileName);
      $urlmonth= str_replace($fileName,"",$file_path_url);
      $subfoldermonth = basename($urlmonth);
      $urlyear = str_replace($subfoldermonth.'/'.$fileName,"",$file_path_url);
      $subfolderyear = basename($urlyear);


    //  $url_folder_dir = str_replace($filename,"",$url);

      $wp_upload_dir = wp_upload_dir();
      $upload_dir = (!empty($wp_upload_dir['basedir'])) ? $wp_upload_dir['basedir'] : "";
      $upload_url = (!empty($wp_upload_dir['baseurl'])) ? $wp_upload_dir['baseurl'] : "";
      if (filter_var($file_path_url, FILTER_VALIDATE_URL) === FALSE) {
        if(file_exists($file_path_url)){
          $upload_dir = str_replace('/'.$subfolderyear.'/'.$subfoldermonth.'/'.$fileName,"",$file_path_url);
        }
      }else{
        $upload_url = str_replace('/'.$subfolderyear.'/'.$subfoldermonth.'/'.$fileName,"",$file_path_url);
      }

    //var_dump($upload_dir);
      $url_folder_dir = $upload_url.'/'.$subfolderyear.'/'.$subfoldermonth.'/';
      $path_folder_dir = $upload_dir.'/'.$subfolderyear.'/'.$subfoldermonth.'/';


      $sizeName = "";
      if(!empty($w)){
        $sizeName = '-'.$w;
      }
      if(!empty($w) && !empty($h)){
        $sizeName = '-'.$w.'x'.$h;
      }

      $file_path_check = $path_folder_dir . $fileName;

      $file_path_check_size = $path_folder_dir . $fileNameNoExtension .$sizeName . '.' .$fileExtension;
      $file_path_check_sub = $path_folder_dir . $fileNameNoExtension .$sizeName. $subfix_name. '.' .$fileExtension;

      $file_path_url = $url_folder_dir . $fileName;
      $file_path_url_size = $url_folder_dir . $fileNameNoExtension .$sizeName. '.' .$fileExtension;
      $file_path_url_sub = $url_folder_dir . $fileNameNoExtension .$sizeName. $subfix_name. '.' .$fileExtension;


        if(file_exists($file_path_check_size)){
            return $file_path_url_size;
        }
        if(file_exists($file_path_check_sub)){
            return $file_path_url_sub;
        }
        if(file_exists($file_path_check) && (!empty($w) && !empty($h))){
          $image = new JEJO_WP_Image_Editor($file_path_check);
          $loadImage = $image->load();
        //   var_dump($pathImg);
          if ( ! is_wp_error( $loadImage) ) {
            $crop = true;
            $image->resize( $w, $h, $crop );
            //$filenamefull = $image->generate_filename($format);
            $returnImg = $image->save( $file_path_check_sub );
            if(!empty($returnImg["file"])){
              return ($returnType != 'url') ? $file_path_check_sub : $file_path_url_sub;
            }
          }
        }


      return $file_path_url;

    }

    /*
     * Create thumb image to hard-disk and get it if it is existed before. the thumb image will be resized and cropped by using WP_Image_Editor class.
     * Limitation: should consider when put width and height for thumb. This function will not delete the unused images.
     * @param $imgID int The id of attachemnt
     * @param $width int The width will be resized
     * @param $height int The height will be resized
     * @param $removeCurrentDomain boolan remove the domain of image
     * @return	$urlReturn string The url of image has been resized
     * @Author: Jenshan
     */

   static private function _thumbImage($imgID,$width,$height,$removeCurrentDomain = false){
     $suFormat = self::$suFormat;
     $urlReturn = "";
     $returnImg = "";
     $pathImg = get_attached_file( $imgID ); /* get local image path */
     $urlReturn = wp_get_attachment_url($imgID);

     if(!empty($pathImg)){
       $urlReturn = self::getThumbnailByURL($pathImg,$width,$height,$urlReturn);
     }
     if($removeCurrentDomain == true){
       $urlReturn = str_replace(site_url(),"",$urlReturn);
     }
     return $urlReturn;
   }


	 /*
	  * Create thumb image to hard-disk and get it if it is existed before. the thumb image will be resized and cropped by using WP_Image_Editor class.
	  * Limitation: should consider when put width and height for thumb. This function will not delete the unused images.
	  * @param $pathImg string The path of image on server
	  * @param $width int The width will be resized
	  * @param $height int The height will be resized
	  * @return	$urlReturn string The url of image has been resized
	  * @Author: Jenshan
	  */

	static private function _thumbImagePath($pathImg,$width,$height, $urlReturn= ""){
		$suFormat = self::$suFormat;
    if(empty($urlReturn)){
      $urlReturn = self::getURLfromFilePath($pathImg);
    }
  //  var_dump($urlReturn);
		if(!empty($pathImg)){
			$ext = pathinfo($pathImg, PATHINFO_EXTENSION);
      $filenamehasExt = basename($pathImg);
      $filenameNoExt = pathinfo($pathImg, PATHINFO_FILENAME );
			$format = $width."x".$height.$suFormat;


			$newFilename = $filenameNoExt.'-'.$format.".".$ext;
      $newURLReturn =  str_replace($filenamehasExt,$newFilename,$urlReturn);
			$newPathImg = str_replace($filenamehasExt,$newFilename,$pathImg);
			if(preg_match('/^'.$format.'/', $pathImg) == false){
				if(!file_exists($newPathImg)){
					$image = new JEJO_WP_Image_Editor($pathImg);
					$loadImage = $image->load();
        //   var_dump($pathImg);
					if ( ! is_wp_error( $loadImage) ) {
						$crop = true;
						$image->resize( $width, $height, $crop );
						//$filenamefull = $image->generate_filename($format);
						$returnImg = $image->save( $newPathImg );

						if(!empty($returnImg["file"])){
              //   var_dump($returnImg);
							$urlReturn = $newURLReturn;
						}
					}
				}else{
					$urlReturn = $newURLReturn;
				}
			}
		}
//    $JWFCommons = new JWFCommons();

		return $urlReturn;
	}

  /*
  * Create thumb image from url .
  * @param $src string The url of attachemnt
  * @param $width int The width will be resized
  * @param $height int The height will be resized
  * @return	$src string The url of image has been resized
  * @Author: Jenshan
  */
  static public function getThumbURLbyPath($pathImg, $width, $height,$haveThumbnail = true){
    $JWFCommons = new JWFCommons();
    $url = "";
    if($JWFCommons->isValidURL($pathImg)){
      $url = $pathImg;
      //$siteURL = get_site_url();
      $pathImg = $JWFCommons->removeURLDomain($pathImg);
      //  var_dump($pathImg);
      $pathImg =  $_SERVER['DOCUMENT_ROOT']. $pathImg;

      //$pathImg = $_SERVER['DOCUMENT_ROOT'].parse_url($pathImg, PHP_URL_PATH);
    }else{
      $url = self::getThumbnailByURL($pathImg, $width, $height, $haveThumbnail);
    }


  //  $path = parse_url('http://domainname/rootfolder/filename.php', PHP_URL_PATH);
    if($haveThumbnail){
      $url = self::getThumbnailByURL($pathImg,$width,$height,$url);
    }

    return $url;
  }

  /*
  * Create thumb image from url .
  * @param $src string The url of attachemnt
  * @param $width int The width will be resized
  * @param $height int The height will be resized
  * @return	$src string The url of image has been resized
  * @Author: Jenshan
  */
  static public function getThumbnailByURL($url, $width, $height, $haveThumbnail = true, $returnType = 'url'){
    $suFormat = self::$suFormat;
    $urlReturn = self::convert_file_path_to_URL($url,$suFormat,$width, $height,true);
///    var_dump($returnImg);exit;
    return $urlReturn;
  }
	  /*
	  * Create thumb image from url .
	  * @param $src string The url of attachemnt
	  * @param $width int The width will be resized
	  * @param $height int The height will be resized
	  * @return	$src string The url of image has been resized
	  * @Author: Jenshan
	  */
    static public function getThumbURL($src, $width, $height, $contentID = ''){
		$imgID = self::getImageIDbyURL($src, $contentID);
		if(!empty($imgID)){
			$src = self::getThumbnailByImgID($imgID, $width, $height);
		}
		return $src;
	}

	/*
	  * Get and create thumb image from Post ID .
	  * @param $postID int The id of Post
	  * @param $width int The width will be resized
	  * @param $height int The height will be resized
	  * @param $haveThumbnail boolan Get under thumb or origin. Default is true for get thumb
	  * @param $removeCurrentDomain boolan remove the domain of image
	  * @return	$src string The url of image has been resized
	  * @Author: Jenshan
	  */
	static public function getThumbnail($postID, $width, $height, $haveThumbnail = true, $removeCurrentDomain = null){
		$imgid=get_post_thumbnail_id($postID);
		if($removeCurrentDomain === null){
			$removeCurrentDomain = self::$removeCurrentDomain;
		}
		$thumbnail = self::getThumbnailByImgID($imgid,$width, $height, $haveThumbnail, $removeCurrentDomain);
		return $thumbnail;
	}

	static public function getThumbnailByImgID($imgid, $width, $height, $haveThumbnail = true, $removeCurrentDomain = null){
		$image = wp_get_attachment_image_src($imgid,'full',false);
		if($removeCurrentDomain === null){
			$removeCurrentDomain = self::$removeCurrentDomain;
		}
		$thumbnail = "";
		if(!empty($image[0])){
			$thumbnail = $image[0];
			if($removeCurrentDomain == true){
				$thumbnail = str_replace(site_url(),"",$thumbnail);
			}
			if($haveThumbnail){
				$curWidth = $image[1];
				$curHeight = $image[2];
				if($curWidth != intval($width) || $curHeight != intval($height)){
					$thumbnail = self::_thumbImage($imgid,$width, $height,$removeCurrentDomain);
				}
			}
		}
		return $thumbnail;
	}


	/*
	  * Get number of rows of attchements from a post
	  * @param $my_id int The id of post
	  * @param $type string A full or partial mime-type, e.g. image, video, video/mp4, which is matched against a post's post_mime_type field
	  * @Author: Jenshan
	  */
	function getLibaryTotal($my_id, $type = 'image'){
		$link = array();
			// from wp-admin/includes/media.php:get_media_items( $post_id, $errors )
			if ( $my_id ) {
			   $attachments = get_children( array( 'post_parent' => $my_id, 'post_type' => 'attachment', 'post_mime_type' => $type, 'orderby' => 'menu_order ASC, ID', 'order' => 'DESC') );
				return count($attachments);

			}
			return 0;
	}


	 /*
	  * Get attchements from a post
	  * @param $my_id int The id of post
	  * @param $type string A full or partial mime-type, e.g. image, video, video/mp4, which is matched against a post's post_mime_type field
	  * @param $limit Limit the amout of return records
	  * @Author: Jenshan
	  */
	static public function getLibary($my_id, $type = 'image', $limit = 0){
		$link = array();

			/* from wp-admin/includes/media.php:get_media_items( $post_id, $errors ) */
			if ( $my_id ) {
				$arg = array( 'post_parent' => $my_id, 'post_type' => 'attachment', 'orderby' => 'menu_order ASC, ID', 'order' => 'DESC');
				if(!empty($type)){
					$arg['post_mime_type'] = $type;
				}
			   $attachments = get_children( $arg );

				$countLimit = 0;
				foreach ( (array) $attachments as $id => $attachment ) {
					if ( $attachment->post_status == 'trash' )
						continue;
					if(!empty($limit))
						if($countLimit == $limit) break;

					$link[$id]["url"] = $attachment->guid;
					$link[$id]["title"] = $attachment->post_title;
					$link[$id]["id"] = $attachment->ID;
					$link[$id]["content"] = $attachment->post_content;
					$link[$id]["createDate"] = $attachment->post_date;
					$link[$id]["updateDate"] = $attachment->post_modified;
					$link[$id]["mineType"] = $attachment->post_mime_type;
					$attachment_fields = get_post_custom( $attachment->ID );
					$link[$id]["linkUrl"] = ( isset($attachment_fields['_linkext_url'][0]) && !empty($attachment_fields['_linkext_url'][0]) ) ? esc_attr($attachment_fields['_linkext_url'][0]) : '';
					$link[$id]["wsAttachementId"] = ( isset($attachment_fields['_ws_attachement_id'][0]) && !empty($attachment_fields['_ws_attachement_id'][0]) ) ? esc_attr($attachment_fields['_ws_attachement_id'][0]) : '';
					$link[$id]["attachmentType"] = ( isset($attachment_fields['_linkext_type'][0]) && !empty($attachment_fields['_linkext_type'][0]) ) ? esc_attr($attachment_fields['_linkext_type'][0]) : 'image';
					switch ($link[$id]["mineType"]){
						case 'video':
						case 'video/mp4':
						case 'video/quicktime':
							$link[$id]["attachmentType"] = 'video';
							$link[$id]["linkUrl"] = $link[$id]["url"];
							break;
						case 'application/msword':
						case 'application/pdf':
						case 'application/excel':
							$link[$id]["attachmentType"] = 'document';
							break;
					}

					$link[$id]["status"] = ( isset($attachment_fields['_file_status'][0]) && !empty($attachment_fields['_file_status'][0]) ) ? esc_attr($attachment_fields['_file_status'][0]) : 'Active';

					/*$type_mine = explode('/',$attachment->post_mime_type);
					$link[$id]["type"] = isset($type_mine[0]) ? $type_mine[0] : '';
					$link[$id]["format"] = isset($type_mine[1]) ? $type_mine[1] : '';*/

					$countLimit ++;
				}

			}
			//echo "<pre>";print_r($link); exit;

			return $link;
	}


	/*
	  * check existing a post mine from attchment of post
	  * @param $my_id int The id of post
	  * @param $type string A full or partial mime-type, e.g. image, video, video/mp4, which is matched against a post's post_mime_type field
	  * @Author: Jenshan
	  */
	function isExistLibary($my_id, $type = 'image'){
			$isExisted = false;
			/* from wp-admin/includes/media.php:get_media_items( $post_id, $errors ) */
			if ( $my_id ) {
			   $attachments = get_children( array( 'post_parent' => $my_id, 'post_type' => 'attachment', 'post_mime_type' => $type, 'orderby' => 'menu_order ASC, ID', 'order' => 'DESC') );

				foreach ( (array) $attachments as $id => $attachment ) {
					if ( $attachment->post_status == 'trash' )
						continue;

					$attachment_fields = get_post_custom( $attachment->ID );
					$videoUrl = ( isset($attachment_fields['_video_url'][0]) && !empty($attachment_fields['_video_url'][0]) ) ? esc_attr($attachment_fields['_video_url'][0]) : '';

					/* $type_mine = explode('/',$attachment->post_mime_type);						*/

					if (!empty($videoUrl)) {
						$isExisted = true ;
						break;
					}

				}

			}
			/* echo "<pre>";print_r($link); */
			return $isExisted;
	}

	function uploadFiles($files, $elementName, $uploads_dir){
		$return = "";
		if(empty($files[$elementName]["error"]) || $files[$elementName]["error"] = 0 ) {
			$tmp_name = $files[$elementName]["tmp_name"];
			$name = date("YmdHis").$files[$elementName]["name"];

			if ($files[$elementName]['error'] === UPLOAD_ERR_OK) {
				$finfo = finfo_open(FILEINFO_MIME_TYPE);
				$mime = finfo_file($finfo, $tmp_name);
				if(!empty($mine)){
					switch ($mime) {
					   case 'application/pdf':
					   case 'application/msword':
					   case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
					   case 'application/x-mswrite':
					   case 'application/rtf':
					   case 'text/richtext':
							if(move_uploaded_file($tmp_name, "$uploads_dir/$name") ){
								$return = $name;
							}
					}
				}else{
					$extArray = explode('.',$files[$elementName]["name"]);
					$count = count($extArray);
					if(!empty($count) && $count > 1){
						$ext = $extArray[$count-1];
						switch ($ext) {
						   case 'doc':
						   case 'docx':
						   case 'pdf':
						   case 'txt':
						   case 'rtf':
						   case 'txt':
								if(move_uploaded_file($tmp_name, "$uploads_dir/$name") ){
									$return = $name;
								}
						}
					}
				}
			}
		}

		return $return;
	}


	function add_custom_mime_types($mimes){
		return array_merge($mimes,array (
			'doc' => 'application/msword',
			'dot' => 'application/msword',
			'docx' => 'application/msword',
			'xls' => 'application/excel',
			'xlsx'  => 'application/excel',
			'mp4|m4v' =>'video/mp4',
			'flv' =>'video/x-flv',
			'mp4|m4v' =>'video/mp4',
			'svg' => 'image/svg+xml'
			));
	}

	static public function insertMediaToPost($filename,$parent_post_id ){

		if(empty($filename) || empty($parent_post_id)) return 0;

		$wp_upload_dir = wp_upload_dir();
		$filenameWP = $wp_upload_dir['path'].'/'.basename( $filename );
		$filenameWPinDB = $wp_upload_dir['subdir'].'/'.basename( $filename );
		$attachmentID = self::getAttachmentIDbyValue($filenameWPinDB);

		// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
		require_once( ABSPATH . 'wp-admin/includes/image.php' );

		if(empty($attachmentID) || $attachmentID == 0){

		// Check the type of file. We'll use this as the 'post_mime_type'.
		$filetype = wp_check_filetype( basename( $filename ), null );

		// Get the path to the upload directory.

		// Prepare an array of post data for the attachment.
		$attachment = array(
			'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ),
			'post_mime_type' => $filetype['type'],
			'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
			'post_content'   => '',
			'post_status'    => 'inherit'
		);
			if(copy($filename, $filenameWP)){

				// Insert the attachment.
				$attachmentID = wp_insert_attachment( $attachment, $filenameWP , $parent_post_id );
			}

		}

		// Generate the metadata for the attachment, and update the database record.
		$attach_data = wp_generate_attachment_metadata( $attachmentID, $filenameWP );
		wp_update_attachment_metadata( $attachmentID, $attach_data );


		return $attachmentID;
	}

	static public function getAttachmentIDbyValue($value){
		$attachID = 0;
		if(empty($value)) return $attachID;

		global $wpdb;
		$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM {$wpdb->prefix}posts WHERE guid RLIKE %s and post_type = 'attachment';", $value ) );
		// Returns null if no attachment is found
		if(!empty($attachment[0])) $attachID = $attachment[0];

		return $attachID;
	}

	static public function getImageIDbyURL( $attachment_url = '' ) {

		global $wpdb;
		$attachment_id = false;

		// If there is no url, return.
		if ( empty($attachment_url))
			return false;

		// Get the upload directory paths
		$upload_dir_paths = wp_upload_dir();
		$attachment_name = "";
		$attachment_url_org = $attachment_url;
		// Make sure the upload path base directory exists in the attachment URL, to verify that we're working with a media library image
		if ( false !== strpos( $attachment_url, $upload_dir_paths['baseurl'] ) ) {

			// If this is the URL of an auto-generated thumbnail, get the URL of the original image

			$attachment_url = preg_replace( '/-\d+x\d+(?=\.(jpg|jpeg|png|gif)$)/i', '', $attachment_url );

			// Remove the upload path base directory from the attachment URL
			$attachment_name = str_replace( $upload_dir_paths['baseurl'] . '/', '', $attachment_url );
		}
		if(empty($attachment_name)){
			$namearr = explode("/",$attachment_url_org);
			$namecount = count($namearr);
			//print_r($namearr);
			if($namecount > 2){
				$filenameLast = $namearr[$namecount-1];
				//$filenameLast = (empty($filenameLast[0])) ? $namearr[$namecount] : $filenameLast[0];
				$attachment_name = $namearr[$namecount-3]."/".$namearr[$namecount-2]."/".$filenameLast;
			}
		}
		//echo $attachment_name;

		if(!empty($attachment_name)){
			$attachment_id = $wpdb->get_var( $wpdb->prepare( "SELECT wposts.ID FROM $wpdb->posts wposts, $wpdb->postmeta wpostmeta WHERE wposts.ID = wpostmeta.post_id AND wpostmeta.meta_key = '_wp_attached_file' AND wpostmeta.meta_value = '%s' AND wposts.post_type = 'attachment'", $attachment_name ) );
		}

		return $attachment_id;
	}

  static public function setAttachmentForFeaturedImageOfPost($postID,$attachmentsid = null){
    $args = array(
    'numberposts' => 1,
    'order'=> 'ASC',
    'post_mime_type' => 'image',
    'post_parent' => $postID,
    'post_type' => 'attachment'
    );
    if(empty($attachmentsid)){
    // Get attachments
      $attachments = get_children( $args );
      if(!empty($attachments)){
        foreach($attachments as $attach)
        {
        // Get only first image
         $attachmentsid = $attach->ID;
         break;
        }
      }
    }
    if(!empty($attachmentsid)){
      // Set Featured image
      $file_path       = get_attached_file( $attachmentsid );
    //  $file_url        = wp_get_attachment_url( $attachmentsid );
      $file_type_data  = wp_check_filetype( basename( $file_path ), null );
      $file_type       = $file_type_data['type'];
      $attachmentsid = self::storeMediaFile( $file_path, $file_type,$postID );
      //restore_current_blog();
      //set_post_thumbnail( $postID, $inserted_attachment_id );
      set_post_thumbnail($postID,$attachmentsid);
      return true;
    }
    return false;
  }

  /**

 */
  static public function getURLfromFilePath($file=''){
    if(empty($file))
      $file = __FILE__;

    // Get correct URL and path to wp-content
    $content_url = untrailingslashit( dirname( dirname( get_stylesheet_directory_uri() ) ) );
    $content_dir = untrailingslashit( WP_CONTENT_DIR );

    // Fix path on Windows
    $file = wp_normalize_path( $file );
    $content_dir = wp_normalize_path( $content_dir );

    $url = str_replace( $content_dir, $content_url, $file );
    return $url;
  }


  /**
	 * Sideload Media File
	 *
	 * @since 0.1.0
	 * @param string $file_url        The URL of the file to sideload.
	 * @param string $file_type       The file type of the file to sideload.
	 * @param int    $timeout_seconds The number of seconds to allow before sideloading times out.
	 * @return array                  On success, returns an associative array of file attributes. On failure, returns
	 *                                $overrides['upload_error_handler'](&$file, $message ) or array( 'error'=>$message ).
	 */
	static public function sideloadMediaFile( $file_url, $file_type, $timeout_seconds ) {
		// Gives us access to the download_url() and wp_handle_sideload() functions.
		require_once( ABSPATH . 'wp-admin/includes/file.php' );
		// Download file to temp dir.
		$temp_file = download_url( $file_url, $timeout_seconds );

		if ( is_wp_error( $temp_file ) ) {
			return false;
			//$temp_file
		}
		// Array based on $_FILE as seen in PHP file uploads.
		$file = array(
			'name'     => basename( $file_url ),
			'type'     => $file_type,
			'tmp_name' => $temp_file,
			'error'    => 0,
			'size'     => filesize( $temp_file ),
		);
		$overrides = array(
			// Tells WordPress to not look for the POST form
			// fields that would normally be present, default is true,
			// we downloaded the file from a remote server, so there
			// will be no form fields.
			'test_form'   => false,
			// Setting this to false lets WordPress allow empty files – not recommended.
			'test_size'   => true,
			// A properly uploaded file will pass this test.
			// There should be no reason to override this one.
			'test_upload' => true,
		);
		// Move the temporary file into the uploads directory.
		return wp_handle_sideload( $file, $overrides );
	}

  /**
	 * Insert media file into the current site
	 *
	 * @since  0.1.0
	 * @param  string $file_path              The path to the media file.
	 * @param  string  $file_type             The mime type of the media file.
	 * @return int    $inserted_attachment_id The inserted attachment ID, or 0 on failure.
	 */
	static public function storeMediaFile( $file_path = '', $file_type = '', $postID = '' ) {
		if ( ! $file_path || ! $file_type ) {
			return;
		}
		// Get the path to the uploads directory.
		$wp_upload_dir = wp_upload_dir();
		// Prepare an array of post data for the attachment.
		//
		$file_name = basename( $file_path );
	  $guid = $wp_upload_dir['url'] . '/' . $file_name ;
	//	$file = $wp_upload_dir['path'].'/'.$file_name;
	//	$filenameWPinDB = $wp_upload_dir['subdir'].'/'.basename( $file_path );
		//$gIDAttachment = self::getAttachmentIDbyValue($filenameWPinDB);
//    $unique_file_name = wp_unique_filename( $upload_dir['path'], $file_name ); // Generate unique name
//    $filenameWP         = basename( $unique_file_name ); // Create image file name

    //$filetype = wp_check_filetype( basename( $file_path ), null );

    //$gIDAttachment = self::getImageIDbyURL($guid);

		$attachment_data = array(
	//		'guid'           => $wp_upload_dir['url'] . '/' . basename( $file_path ),
			'post_mime_type' => $file_type,
			'post_title'     => sanitize_file_name( $file_name ),
			'post_content'   => '',
			'post_status'    => 'inherit',
		);
    if(!empty($postID)){
      $attachment_data['post_parent'] = $postID;
    }
    if(empty($gIDAttachment) || $gIDAttachment == 0){
		// Insert the attachment.
		$inserted_attachment_id   = wp_insert_attachment( $attachment_data, $file_path,$postID);
		//$inserted_attachment_path = get_attached_file( $inserted_attachment_id );
		// Update the attachment metadata.
		self::update_inserted_attachment_metadata( $inserted_attachment_id, $file_path );
    $gIDAttachment = $inserted_attachment_id;
    }

		return $gIDAttachment;
	}
	/**
	 * Update inserted attachment metadata
	 *
	 * @since 0.1.0
	 * @param int    $inserted_attachment_id The inserted attachment ID.
	 * @param string $file_path              The path to the media file.
	 */
	static private function update_inserted_attachment_metadata( $inserted_attachment_id, $file_path ) {
		// Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
		require_once( ABSPATH . 'wp-admin/includes/image.php' );
		// Generate metadata for the attachment and update the database record.
		$attach_data = wp_generate_attachment_metadata( $inserted_attachment_id, $file_path );
		wp_update_attachment_metadata( $inserted_attachment_id, $attach_data );
	}
  /**
  * find files matching a pattern
   * using unix "find" command
   *
   * @return array containing all pattern-matched files
   *
   * @param string $dir     - directory to start with
   * @param string $pattern - pattern to search
   */
  	static public function find($dir, $pattern, $use_glob = false){
    // escape any character in a string that might be used to trick
    // a shell command into executing arbitrary commands
    $dir = escapeshellcmd($dir);
    if(!function_exists("shell_exec")){
      $use_glob = true;
    }

    // execute "find" and return string with found files
    if(!($use_glob)){
      $files = shell_exec("find $dir -name '$pattern' -print");
      // create array from the returned string (trim will strip the last newline)
      $files = explode("\n", trim($files));
    }else{
          // get a list of all matching files in the current directory
        $files = glob("$dir/$pattern");
        // find a list of all directories in the current directory
        // directories beginning with a dot are also included
        foreach (glob("$dir/{.[^.]*,*}", GLOB_BRACE|GLOB_ONLYDIR) as $sub_dir){
            $arr   = find($sub_dir, $pattern, $use_glob);  // resursive call
            $files = array_merge($files, $arr); // merge array with files from subdirectory
        }
    }
    // return array
    return $files;
}




}
?>
