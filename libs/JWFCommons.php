<?php
class JWFCommons {

    public $enableThumbnails = array('post', 'page');
    private $listCSS = array('/assets/admin/admin.css');
    private $listCSSMain = array('/assets/admin/main-admin.css');
    private $listJS = array('/assets/admin/admin.js');
    private $favicon = '';
    private $unDeletePosts = array();
    private $adminFooterText = '';
    private $hideEditorForPageTemplates = array();
    static $instance;
    private $categoryIDs = array();
    public $WPMU_enableUserEditForAdmin = true;

    public function __construct($categoryIDs = array()) {
      if(!empty($categoryIDs)){
        $this->categoryIDs = $categoryIDs;
        if ($this->isCategoryDeleteRequest()) {
          add_filter('check_admin_referer', array($this, 'check_referrer'), 10, 2);
          add_filter('check_ajax_referer', array($this, 'check_referrer'), 10, 2);
        }
      }
    }

    public function init() {
        //add_action('init', array($this, 'hvn_rewrite_system'));
		    add_theme_support('post-thumbnails');
        add_post_type_support('page', 'excerpt');
        add_filter('excerpt_more', array($this, 'excerptMore'));
        add_filter('upload_mimes', array($this, 'filterMymeTypes'), 1, 1);

        // turn off message for update wordpress version
      //  add_filter('pre_site_transient_update_core', create_function('$a', "return null;"));
        //add_action('template_redirect', array($this, 'load_jquery'));

        add_action('admin_bar_menu', array($this, 'fb_admin_bar_menu'), 25);
        add_filter('update_footer', array($this, 'change_footer_version'), 9999);

        add_filter('admin_title', array($this, 'my_admin_title'), 10, 2);
        add_action('widgets_init', array($this, 'unregister_default_widgets'), 11);
        if($this->WPMU_enableUserEditForAdmin){
          add_filter( 'map_meta_cap', array($this, 'wpmu_mc_admin_users_caps'), 1, 4 );
          remove_all_filters( 'enable_edit_any_user_configuration' );
          add_filter( 'enable_edit_any_user_configuration', '__return_true');
          add_filter( 'admin_head', array($this,'wpmu_mc_edit_permission_check')  , 1, 4 );
        }
      //  if(is_multisite()){
        //    add_filter('upload_dir', array($this,'ms_fix_upload_paths'));
      //  }
      add_action( 'admin_init', array($this,'admin_notice_message_important' ));
      add_action( 'admin_menu', array($this,'allow_menu_editor'));
	}

  public function allow_menu_editor() {
    $arr_menu = array_filter(get_nav_menu_locations());
    if ( ! empty($arr_menu) ) {
        $obj_role = get_role('editor');
        $obj_role->add_cap('edit_theme_options');
    }
  }


  public function admin_notice_message_important() {
    if( ! wp_doing_ajax() ){
    $my_theme = wp_get_theme( 'nashtech' );
    if ( $my_theme->exists() ){
      $theme_name =  esc_html( $my_theme );
      $revision = WP_POST_REVISIONS;
    //  var_dump($revision);
      if(empty($revision)){
	       $class = 'notice notice-warning';
	       $message = __( '<strong>'.$theme_name.'</strong>: The revision of posts has been disabled for development. Please enable it in <strong> wp-config.php </strong> for production environment.', 'pitalent.com' );
	        printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ),  $message  );
      }
    }
    }
  }

  public function ms_fix_upload_paths($data)
  {
    $siteID = get_current_blog_id();
    if(!is_main_site($siteID)){
      $data['basedir'] = $data['basedir'].'/sites/'.get_current_blog_id();
      $data['path'] = $data['basedir'].$data['subdir'];
      $data['baseurl'] = $data['baseurl'].'/sites/'.get_current_blog_id();
      $data['url'] = $data['baseurl'].$data['subdir'];
    }
      return $data;
  }

  function hide_editor() {
          $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
          if( !isset( $post_id ) ) return;
          $template_file = get_post_meta($post_id, '_wp_page_template', true);
  	$array_template = $this->hideEditorForPageTemplates;
    $templateDB = str_replace('/',"-",$template_file);
    $templateCFG = str_replace('/',"-",$array_template);
      if(in_array($templateDB,$templateCFG)	)
  	{ // edit the template name
          remove_post_type_support('page', 'editor');
      }
  }

	function add_my_favicon() {
		$favicon_path = $this->favicon;
		echo '<link rel="shortcut icon" href="' . $this->favicon . '" />';
	}

	public function addFavicon ($path){
		$this->favicon = $path;
		add_action( 'wp_head', array($this,'add_my_favicon' )); //front end
		add_action( 'admin_head', array($this,'add_my_favicon' )); //admin end
	}

	public function turnOffCoreUpdate(){
		add_action('after_setup_theme',array($this,'remove_core_updates'));
		add_filter('pre_site_transient_update_core',array($this,'remove_core_updates'));
		add_filter('pre_site_transient_update_plugins',array($this,'remove_core_updates'));
		add_filter('pre_site_transient_update_themes',array($this,'remove_core_updates'));
	}

	public function turnOffPluginUpdate(){
		remove_action('load-update-core.php',array($this,'wp_update_plugins'));
		add_filter('pre_site_transient_update_plugins','__return_null');
	}

	function remove_core_updates()
	{
	 if(! current_user_can('update_core')){return;}
	 add_action('init', create_function('$a',"remove_action( 'init', 'wp_version_check' );"),2);
	 add_filter('pre_option_update_core','__return_null');
	 add_filter('pre_site_transient_update_core','__return_null');
	}

	public function autoUpdatePlugin($status = true){
		if($status === false){
			add_filter( 'auto_update_plugin', '__return_false' );
	   }
	}

	public function activePHPExcel(){
		require_once dirname( __FILE__ ).'/../3rd/phpexcel/PHPExcel/IOFactory.php';
 	}

	public function disableComment(){
		add_action( 'admin_menu', array($this,'comment_remove_admin_menus') );
		add_action('init', array($this,'comment_remove_support'), 100);
		add_action( 'wp_before_admin_bar_render', array($this,'comment_mytheme_admin_bar_render') );
	}


	function comment_remove_admin_menus() {
		remove_menu_page( 'edit-comments.php' );
	}

	function comment_remove_support() {
		$agrs=array("post","page");
		foreach ($agrs as $type){
			remove_post_type_support( $type, 'comments' );
		}
	}

	function comment_mytheme_admin_bar_render() {
		global $wp_admin_bar;
		$wp_admin_bar->remove_menu('comments');
	}

	/*
	* create Menu
	* $agrs: is array
	  e.g: array('headermenu' => 'Header Menu', 'footermenu'=>'Footer Menu')
	*/
	public function createMenu ($agrs){
		register_nav_menus($agrs);
	}

	public function addCssToRTE ($list){
		$this->listCSS = $list;
		add_filter('mce_css', array($this, 'addCssAdmin'));

	}

	public function addCssToAdmin($list) {
		$this->listCSSMain = $list;
		add_action( 'admin_print_styles', array($this,'mytheme_add_init') );
    }

	public function addJsToAdmin ($list){
		$this->listJS = $list;
		add_action('admin_enqueue_scripts', array($this,'add_admin_js'));

	}

	public function unDeletePostIDs ($args){
		  $this->unDeletePosts = $args;
	    add_action( 'before_delete_post', array($this,'hook_delete_post') );
	}

  public function hideEditorForPageTemplates ($args){
      if(!empty($args) && is_array($args)){
        $this->hideEditorForPageTemplates = $args;
        add_action( 'admin_init', array($this,'hide_editor' ));
      }
      //add_action( 'before_delete_post', array($this,'hook_delete_post') );
  }

	public function changeAdminFooterText ($text){
		$this->adminFooterText = $text;
		add_filter('admin_footer_text', array($this, 'admin_footer_text'));
	}

	function admin_footer_text() {
		echo $this->adminFooterText;
	}

	function addCssAdmin($wp) {
		foreach($this->listCSS as $css)	{
			$wp .= ',' . get_template_directory_uri() . $css;
		}
        return $wp;
    }


	function hook_delete_post( $postid ){
		// We check if the global post type isn't ours and just return
		global $post_type, $post;

		foreach ($this->unDeletePosts as $postType => $postList){
				if ( $post_type == $postType && in_array($post->ID, $postList) ) {
					echo "This is system article.  You can not delete it!";
					exit;
				}
		}
		return;

		// My custom stuff for deleting my custom post type here
	}


    function add_admin_js() {
        $file_dir = get_template_directory_uri();
		foreach($this->listJS as $key => $js)	{
			wp_enqueue_script("jwf-common-admin-js-".$key, $file_dir . $js);
		}
    }

	function mytheme_add_init() {
        $file_dir = get_template_directory_uri();
		foreach($this->listCSSMain as $key => $listCSSMain)	{
			wp_register_style("jwf-common-admin-css-".$key, $file_dir . $listCSSMain, false, "1.0", "all");
			wp_enqueue_style("jwf-common-admin-css-".$key);
		}
    }

    function load_jquery() {
        wp_deregister_script('jquery');
    }

     function unregister_default_widgets() {
      //  unregister_widget('WP_Widget_Pages');
        unregister_widget('WP_Widget_Calendar');
        unregister_widget('WP_Widget_Archives');
       // unregister_widget('WP_Widget_Links');
        unregister_widget('WP_Widget_Meta');
        unregister_widget('WP_Widget_Search');
       // unregister_widget('WP_Widget_Text');
        unregister_widget('WP_Widget_Categories');
        unregister_widget('WP_Widget_Recent_Posts');
        unregister_widget('WP_Widget_Recent_Comments');
        unregister_widget('WP_Widget_RSS');
        unregister_widget('WP_Widget_Tag_Cloud');
       // unregister_widget('WP_Nav_Menu_Widget');
        unregister_widget('Twenty_Eleven_Ephemera_Widget');
        unregister_widget('Akismet_Widget');
        unregister_widget('qTranslateWidget');
        unregister_widget('GADWidget');
    }

    function my_admin_title($admin_title, $title) {
        if (!empty($title)) {
            return get_bloginfo('name') . ' &#8212; ' . $title;
        }
        return ' &lsaquo; ' . get_bloginfo('name');
    }

    // remove footer
    function change_footer_version() {
        return ' ';
    }



    // remove wp logo on bar
    function fb_admin_bar_menu($admin_bar) {
        // remove wp-logo for each menu on menu bar
        if(is_multisite()){
        $blogs = $this->getBlogIDList();
        if(!empty($blogs)){
          foreach ($blogs as $blogId) {
            $node = $admin_bar->get_node('blog-' . $blogId);
            if ($node) {
                $node->title = strip_tags($node->title);
                $admin_bar->add_node($node);
            }
          }
        }
        }
        // remove wp logo
        $admin_bar->remove_node('wp-logo');
    }

    // remove read more
    function excerptMore($more) {
        global $post;
        return '';
    }

    /*
     * Limit char amount of string
     * @Author: Jenshan
     */

    static public function getMaxChar($string, $charlength, $return = false, $more = '...') {
        $charlength++;
        if (mb_strlen($string) > $charlength) {
            $subex = mb_substr($string, 0, $charlength - 5);
            $exwords = explode(' ', $subex);
            $excut = - ( mb_strlen($exwords[count($exwords) - 1]) );
            if ($excut < 0) {
                $data = mb_substr($subex, 0, $excut) . $more;
                if ($return)
                    return $data;
                else
                    echo $data;
            } else {
                $subex .= $more;
                if ($return)
                    return $subex;
                else
                    echo $subex;
            }
        } else {
            if ($return)
                return $string;
            else
                echo $string;
        }
    }


    /**
     * Get list of blogs
     */
    public function getBlogList() {
        $sites = array();
		if(is_multisite()){
        if ( function_exists( 'get_sites' ) && class_exists( 'WP_Site_Query' ) ) {
          $blogs = get_sites();

  			foreach ( $blogs as $site ) {
  				$sites[$site->blog_id] = get_blog_option($site->blog_id, 'blogname');
  			}
  			natsort($sites);
  		}
    }
        return $sites;
    }

    public function getBlogIDList() {
		$sites = array();
		if(is_multisite()){
      if ( function_exists( 'get_sites' ) && class_exists( 'WP_Site_Query' ) ) {
  			$blogs = get_sites();
  			foreach ($blogs as $blog) {
  			   $sites[] = $blog->blog_id;
  			}
  		}
    }
        return $sites;
    }
    public function removeURLDomain($url){
      if(is_multisite()){
        if ( function_exists( 'get_sites' ) && class_exists( 'WP_Site_Query' ) ) {
    			$blogs = get_sites();
          $siteURLs =  array();
          $siteURLsR = array();
    			foreach ($blogs as $blog) {
    			   $siteURLs[] = get_site_url($blog->blog_id);
    			}
          rsort($siteURLs);
          $url =  str_replace($siteURLs,"",$url);

    		}
      }else{
        $siteURLs = home_url();
        $url  = str_replace($siteURLs,"",$url);
      }
      return $url;
    }

    function doUploadFiles($files, $elementName, $uploads_dir, $prefix) {
        $return = "";
        if (empty($files[$elementName]["error"]) || $files[$elementName]["error"] = 0) {
            $tmp_name = $files[$elementName]["tmp_name"];
            $name = date("YmdHis") . $prefix . $files[$elementName]["name"];

            if ($files[$elementName]['error'] === UPLOAD_ERR_OK) {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mime = finfo_file($finfo, $tmp_name);
                if (!empty($mine)) {
                    switch ($mime) {
                        case 'application/pdf':
                        case 'application/msword':
                        case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
                        case 'application/x-mswrite':
                        case 'application/rtf':
                        case 'text/richtext':
                        case 'text/rtf':
                            if (move_uploaded_file($tmp_name, "$uploads_dir/$name")) {
                                $return = $name;
                            } else {
                                if (copy($tmp_name, "$uploads_dir/$name")) {
                                    $return = $name;
                                }
                            }
                    }
                } else {
                    $extArray = explode('.', $files[$elementName]["name"]);
                    $count = count($extArray);
                    if (!empty($count) && $count > 1) {
                        $ext = $extArray[$count - 1];
                        switch ($ext) {
                            case 'doc':
                            case 'docx':
                            case 'pdf':
                            case 'txt':
                            case 'rtf':
                            case 'txt':
                                if (move_uploaded_file($tmp_name, "$uploads_dir/$name")) {
                                    $return = $name;
                                } else {
                                    if (copy($tmp_name, "$uploads_dir/$name")) {
                                        $return = $name;
                                    }
                                }
                        }
                    }
                }
            }
        }

        return $return;
    }

    function filterMymeTypes($mime_types) {
        unset($mime_types['bmp']); //Removing the bmp extension
        return $mime_types;
    }

    function getPageIdBySlug($page_slug) {
        $page = get_page_by_path($page_slug);
        if ($page) {
            return $page->ID;
        } else {
            return null;
        }
    }

    function getAPIs($url) {
        $ch = curl_init();
        $timeout = 0; // set to zero for no timeout
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE); // set to true to see the header
        curl_setopt($ch, CURLOPT_NOBODY, FALSE); // show the body
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $file_contents = curl_exec($ch); // take out the spaces of curl statement!!
        curl_close($ch);
        $jsondecode = array();
        $data = array();
        if (!empty($file_contents)) {
            $jsondecode = json_decode($file_contents);
            if (!empty($jsondecode)) {
                if (!empty($jsondecode->dataReturn)) {
                    $data = $jsondecode->dataReturn;
                } else {
                    echo "<!-- " . $jsondecode->strMessageReturn . " -->";
                }
            } else {
                echo "<!-- Object data is null -->";
            }
        }
        return $data;
    }

    function postAPIs($url,$params)
    {
    	$postData = '';
    	//create name value pairs seperated by &
    	foreach($params as $k => $v)
    	{
    		$postData .= $k . '='.$v.'&';
    	}
    	$postData = rtrim($postData, '&');
    	$ch = curl_init();


    	curl_setopt($ch,CURLOPT_URL,$url);
    	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    	curl_setopt($ch,CURLOPT_HEADER, false);
    	curl_setopt($ch, CURLOPT_POST, count($postData));
    	curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    	$output=curl_exec($ch);


    	curl_close($ch);
    	return $output;
    }

    function getJSON_URL($url) {
        $ch = curl_init();
        $timeout = 0; // set to zero for no timeout
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, FALSE); // set to true to see the header
        curl_setopt($ch, CURLOPT_NOBODY, FALSE); // show the body
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $file_contents = curl_exec($ch); // take out the spaces of curl statement!!
        curl_close($ch);
        $jsondecode = array();
        $data = array();
        if (!empty($file_contents)) {
            $jsondecode = json_decode($file_contents);
        }
        return $jsondecode;
    }

    function dumpArray($var, $exitpage = true) {
        echo "<pre>";
        print_r($var);
        if ($exitpage)
            exit;
    }

    function parseYoutubeURL($url) {
        $pattern = '#^(?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})(?:.+)?$#x';
        preg_match($pattern, $url, $matches);
        return (isset($matches[1])) ? $matches[1] : false;
    }

    function check_user_role($roles, $user_id = NULL) {
        // Get user by ID, else get current user
        if ($user_id)
            $user = get_userdata($user_id);
        else
            $user = wp_get_current_user();

        // No user found, return
        if (empty($user))
            return FALSE;

        // Append administrator to roles, if necessary
        if (!in_array('administrator', $roles))
            $roles[] = 'administrator';

        // Loop through user roles
        foreach ($user->roles as $role) {
            // Does user have role
            if (in_array($role, $roles)) {
                return TRUE;
            }
        }

        // User not in roles
        return FALSE;
    }

    /**
     * Get feeds
     * @param string $feed_url
     */
    function getFeed($feed_url, $name, $max = 2) {
        $feeds = '<div class="nt-box-article clearfix">' . $name;
        $content = file_get_contents($feed_url);
        $x = new SimpleXmlElement($content);
        $i = 1;
        foreach ($x->channel->item as $entry) {
            $feeds .= "<div><a href='$entry->link' title='$entry->title'>" . $entry->title . "</a></div>";
            if ($i == $max)
                break;
            $i++;
        }
        $feeds .='</div>';
        return $feeds;
    }


    /**
     * Get summary of a string
     * @param type $string
     * @param type $length
     */
    static public function getSummary($string, $length = 30,$display = true) {
        $string = wp_strip_all_tags($string);
        if(strlen($string) < $length) return $string;
        $arrName = explode(' ', substr($string, 0, $length));
        array_pop($arrName);
		if($display === true) echo implode(' ', $arrName) . '...';
		else return implode(' ', $arrName). '...';
    }

    /**
     * Tweets
     */
    function buildBaseString($baseURI, $method, $params) {
        $r = array();
        ksort($params);
        foreach ($params as $key => $value) {
            $r[] = "$key=" . rawurlencode($value);
        }
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    }

    function buildAuthorizationHeader($oauth) {
        $r = 'Authorization: OAuth ';
        $values = array();
        foreach ($oauth as $key => $value)
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        $r .= implode(', ', $values);
        return $r;
    }

    function getJson($cacheFile, $input) {
		$cacheFile = ABSPATH .'cache/'. $cacheFile;
        if (file_exists($cacheFile)) {
            $fh = fopen($cacheFile, 'r');
            $cacheTime = trim(fgets($fh));

            // if data was cached recently, return cached data
            if ($cacheTime > strtotime('-5 minutes')) {
                return fread($fh, filesize($cacheFile));
            }

            // else delete cache file
            fclose($fh);
            unlink($cacheFile);
        }

        $json = JWPCommons::getTimeline($input);

        $fh = fopen($cacheFile, 'w');
        fwrite($fh, time() . "\n");
        fwrite($fh, $json);
        fclose($fh);

        return $json;
    }

    function getTimeline($input) {
        $url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
        extract($input);

        $oauth = array('oauth_consumer_key' => $consumer_key,
            'oauth_nonce' => time(),
            'oauth_signature_method' => 'HMAC-SHA1',
            'oauth_token' => $oauth_access_token,
            'oauth_timestamp' => time(),
            'oauth_version' => '1.0');

        $base_info = JWPCommons::buildBaseString($url, 'GET', $oauth);
        $composite_key = rawurlencode($consumer_secret) . '&' . rawurlencode($oauth_access_token_secret);
        $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
        $oauth['oauth_signature'] = $oauth_signature;

        // Make Requests
        $header = array(JWPCommons::buildAuthorizationHeader($oauth), 'Expect:');
        $options = array(CURLOPT_HTTPHEADER => $header,
            //CURLOPT_POSTFIELDS => $postfields,
            CURLOPT_HEADER => false,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false);

        $feed = curl_init();
        curl_setopt_array($feed, $options);
        $json = curl_exec($feed);
        curl_close($feed);

        //$twitter_data = json_decode($json);
        return $json;
    }


	function isValidURL($url)
	{

    if (!filter_var($url, FILTER_VALIDATE_URL) === false) {
      return true;
    }
    return false;

		//return preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $url);
	}

	public function recursive_array_replace($find, $replace, $array){
        if (!is_array($array)) {
        return str_replace($find, $replace, $array);
        }
        $newArray = array();
        foreach ($array as $key => $value) {
			$newArray[$key] = $this->recursive_array_replace($find, $replace, $value);
        }
        return $newArray;
    }

	static public function formatCurrency($number, $currency = 'VND', $decimals = 0){
		if(trim($number) != "" || $number != null){
			$number = number_format($number, $decimals, ',', '.').' '.$currency;
		}
		return $number;
	}
	static public function jsRedirect($url){
		echo "<script>window.location = '".$url."';</script>";
	}


	static public function str_to_csv( $row )
	{
		if( $row=='' )
		{
			return array();
		}
		$a = array();
		$src = explode(',', $row );
		do{
			$p = array_shift($src);
			while( mb_substr_count($p,'"') % 2 != 0 )
			{
				if( count($src)==0 ){ return false;	}
				$p .= ','.array_shift($src);
			}
			$match = null;
			if( preg_match('/^"(.+)"[
	]*$/', $p, $match ))
			{
				$p = $match[1];
			}
			$a[] = str_replace('""','"',$p);
		}while( count($src) > 0 );
		return $a;
	}


	static public function file_getcsv( $f )
	{
		$line = fgets( $f );
		while( ($a = self::str_to_csv($line))===false )
		{
			if( feof($f) ){	return false; }
			$line .= "\n".fgets( $f );
		}
		return $a;
	}


	static public function readCSV( $filename )
	{
		ini_set("auto_detect_line_endings", true);
		$a = array();
		$f = fopen($filename,'r');
		while( !feof($f) )
		{
			$rec = self::file_getcsv($f);
			if( $rec===false ){	return false; }
			if( !empty($rec) )
			{
				$a[] = $rec;
			}
		}
		fclose($f);
		return $a;
	}

	static public function multi_explode($delimiters,$string) {
		$ready = str_replace($delimiters, $delimiters[0], $string);
		$launch = explode($delimiters[0], $ready);
		return  $launch;
	}
static public function timeAgo($timestamp,$subtext){
      return JWFDateTime::timeAgo($timestamp,$subtext);
}
public static function unDeleteCategoryIDs(array $categoryIDs) {
		if (null===self::$instance) {
			self::$instance = new self($categoryIDs);
		} else {
			throw new BadFunctionCallException(sprintf('Plugin %s already instantiated', __CLASS__));
		}
		return self::$instance;
	}
	private function isCategoryDeleteRequest() {
		$notAnCategoryDeleteRequest =
			empty($_REQUEST['taxonomy'])
			|| empty($_REQUEST['action'])
			|| $_REQUEST['taxonomy'] !== 'category'
			|| !( $_REQUEST['action'] === 'delete' || $_REQUEST['action'] === 'delete-tag');
		$isCategoryDeleteRequest = !$notAnCategoryDeleteRequest;

		return $isCategoryDeleteRequest;
	}

	private function blockCategoryID($categoryID) {
		return in_array($categoryID, $this->categoryIDs);
	}
	/**
	 * @-wp-hook check_admin_referer
	 * @-wp-hook check_ajax_referer
	 */
	public function check_referrer($action, $result) {
		if (!$this->isCategoryDeleteRequest()) {
			return;
		}
		$prefix = 'delete-tag_';
		if (strpos($action, $prefix) !== 0)
			return;
		$actionID = substr($action, strlen($prefix));
		$categoryID = max(0, (int) $actionID);
		if ($this->blockCategoryID($categoryID)) {
			wp_die(__('This category is blocked for deletion.'));
		}
	}

public static function filterKeywords($keywords) {
  $keywords = preg_replace('/[\pP]/u', '', trim(preg_replace('/\s\s+/iu', '', $keywords)));
  return $keywords;
}

public function wpmu_mc_admin_users_caps( $caps, $cap, $user_id, $args ){

    foreach( $caps as $key => $capability ){

        if( $capability != 'do_not_allow' )
            continue;

        switch( $cap ) {
            case 'edit_user':
            case 'edit_users':
                $caps[$key] = 'edit_users';
                break;
            case 'delete_user':
            case 'delete_users':
                $caps[$key] = 'delete_users';
                break;
            case 'create_users':
                $caps[$key] = $cap;
                break;
        }
    }

    return $caps;
}


/**
 * Checks that both the editing user and the user being edited are
 * members of the blog and prevents the super admin being edited.
 */
public function wpmu_mc_edit_permission_check() {
    global $current_user, $profileuser;

    $screen = get_current_screen();

    wp_get_current_user();

    if( ! is_super_admin( $current_user->ID ) && in_array( $screen->base, array( 'user-edit', 'user-edit-network' ) ) ) { // editing a user profile
        if ( is_super_admin( $profileuser->ID ) ) { // trying to edit a superadmin while less than a superadmin
            wp_die( __( 'You do not have permission to edit this user.' ) );
        } elseif ( ! ( is_user_member_of_blog( $profileuser->ID, get_current_blog_id() ) && is_user_member_of_blog( $current_user->ID, get_current_blog_id() ) )) { // editing user and edited user aren't members of the same blog
            wp_die( __( 'You do not have permission to edit this user.' ) );
        }
    }

}


}
